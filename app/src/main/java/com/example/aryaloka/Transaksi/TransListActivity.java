package com.example.aryaloka.Transaksi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aryaloka.Model.master.MasterKaryawanModel;
import com.example.aryaloka.Model.transaksi.TrHistCustModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.example.aryaloka.Utilities.Utils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class TransListActivity extends AppCompatActivity implements OnMapReadyCallback, RatingDialogListener {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level;
    private ImageView imgBack;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private TextView txtNoTrans, txtTglTrans, txtbarang, txtKeluhan, txtStatusProgress, txtTeknisi, txtPerikiraanBiaya, txtTglAmbil,
        txtPerkiraanHariSelesai, txtCatatan, txtBiaya, txtNilaiPromo, txtNetto, txtTglSelesai, txtTglAntar, txtTglGaransi,
            txtAlamat, txtRef;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private static final long INTERVAL = 1000 * 2 * 1; //2 detik
    private static final long FASTEST_INTERVAL = 1000 * 1 * 1; // 1 detik
    private GoogleMap mGoogleMap;
    private TrHistCustModel model;
    private float zoomLevel = (float) 16.0;
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
    private Button btnChat, btnProfile, btnUlasan, btnKomplain;
    private MasterKaryawanModel karyawan;
    private double rating;
    private Date now = Calendar.getInstance().getTime();

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trans_item_view_activity);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}
        Intent i = getIntent();
        if (i != null){
            try {
                model = (TrHistCustModel) i.getSerializableExtra("model");
            }catch (Exception e) {}
        }
        karyawan = model.getKaryModel();
        imgBack = (ImageView)findViewById(R.id.ImgBackInfoTransaksi);
        txtNoTrans = (TextView)findViewById(R.id.txtNomorTrInfoTransaksi);
        txtRef = (TextView)findViewById(R.id.txtNomorRefTrInfoTransaksi);
        txtTglTrans = (TextView)findViewById(R.id.txtTglTrInfoTransaksi);
        txtbarang = (TextView)findViewById(R.id.txtBarangInfoTransaksi);
        txtKeluhan = (TextView)findViewById(R.id.txtKeluhanInfoTransaksi);
        txtStatusProgress = (TextView)findViewById(R.id.txtStatusProgressInfoTransaksi);
        txtTeknisi = (TextView)findViewById(R.id.txtTeknisiInfoTransaksi);
        txtPerikiraanBiaya = (TextView)findViewById(R.id.txtPerkiraanBiayaInfoTransaksi);
        txtPerkiraanHariSelesai = (TextView)findViewById(R.id.txtPerkiraanHariSelesaiInfoTransaksi);
        txtCatatan = (TextView)findViewById(R.id.txtCatatanInfoTransaksi);
        txtBiaya = (TextView)findViewById(R.id.txtBiayaFinalInfoTransaksi);
        txtNilaiPromo = (TextView)findViewById(R.id.txtNilaiPromoInfoTransaksi);
        txtNetto = (TextView)findViewById(R.id.txtNettoInfoTransaksi);
        txtTglSelesai = (TextView)findViewById(R.id.txtTglSelesaiInfoTransaksi);
        txtTglAmbil = (TextView)findViewById(R.id.txtTglAmbilInfoTransaksi);
        txtTglAntar = (TextView)findViewById(R.id.txtTglPengantaranInfoTransaksi);
        txtTglGaransi = (TextView)findViewById(R.id.txtTglGaransiInfoTransaksi);
        txtAlamat = (TextView)findViewById(R.id.txtAlamatInfoTransaksi);
        btnChat = (Button)findViewById(R.id.btnChatTeknisiInfoTransaksi);
        btnProfile = (Button)findViewById(R.id.btnProfileTeknisiInfoTransaksi);
        btnUlasan = (Button)findViewById(R.id.btnUlasanInfoTransaksi);
        btnKomplain = (Button)findViewById(R.id.btnKomplainInfoTransaksi);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.frgMapsInfoTransaksi);
        fm.getMapAsync(this);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getKodeUserTeknisi()==null){
                    Toast.makeText(TransListActivity.this, "Teknisi belum tersedia", Toast.LENGTH_LONG).show();
                }else {
                    Intent i = new Intent(TransListActivity.this, TransChatActivity.class);
                    i.putExtra("noTrans", model.getNoTrans());
                    i.putExtra("idUser", model.getKodeUserCust());
                    i.putExtra("idTeknisi", model.getKodeUserTeknisi());
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                //https://github.com/DeKoServidoni/FirebaseChatAndroid
            }
        });

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getKodeUserTeknisi()==null){
                    Toast.makeText(TransListActivity.this, "Teknisi belum tersedia", Toast.LENGTH_LONG).show();
                }else {
                    Intent i = new Intent(TransListActivity.this, ProfileTeknisiActivity.class);
                    i.putExtra("karyawan", karyawan);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        });

        btnUlasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getTglAntar()!=null){
                    if(model.getCountUlasan().intValue()>0){
                        Toast.makeText(TransListActivity.this, "Anda sudah berpartisipasi memberikan ulasan", Toast.LENGTH_LONG).show();
                    }else{
                        showDialogRate();
                    }
                }else{
                    Toast.makeText(TransListActivity.this, "Transaksi belum selesai. Tidak bisa memberikan ulasan.", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnKomplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if(model.getTglBatasGaransi()==null){
                        Toast.makeText(TransListActivity.this, "Masih belum tersedia untuk pengajuan komplain", Toast.LENGTH_LONG).show();
                    }else{
                        Date tgl = df.parse(model.getTglBatasGaransi());
                        if(Utils.getLastTimeOfDay(tgl).compareTo(now) > 0){
                            Intent i  = new Intent(TransListActivity.this, TransGaransiActivity.class);
                            i.putExtra("model", model);
                            startActivityForResult(i, 8);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }else{
                            Toast.makeText(TransListActivity.this, "Masa berlaku garansi sudah habis!", Toast.LENGTH_LONG).show();
                        }
                    }
                }catch (Exception ex){
                    Toast.makeText(TransListActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        btnUlasan.setVisibility(View.GONE);
        btnKomplain.setVisibility(View.GONE);

        if(model.getKodeUserTeknisi() == null){
            btnProfile.setVisibility(View.GONE);
            btnChat.setVisibility(View.GONE);
        }else{
            btnProfile.setVisibility(View.VISIBLE);
            btnChat.setVisibility(View.VISIBLE);
        }

        Date tglTrans = Calendar.getInstance().getTime();
        Date tglAntar = Calendar.getInstance().getTime();
        Date tglSelesai = Calendar.getInstance().getTime();
        Date tglGaransi = Calendar.getInstance().getTime();
        Date tglAmbil = Calendar.getInstance().getTime();
        try{
            tglTrans = df.parse(model.getTglTrans());
            if(model.getTglSelesai()!=null)
                tglSelesai = df.parse(model.getTglSelesai());
            if(model.getTglBatasGaransi()!=null) {
                tglGaransi = df.parse(model.getTglBatasGaransi());
                if(Utils.getFirstTimeOfDay(tglAntar).compareTo(now) < 0 && Utils.getLastTimeOfDay(tglGaransi).compareTo(now) > 0){
                    btnKomplain.setVisibility(View.VISIBLE);
                }else{
                    btnKomplain.setVisibility(View.GONE);
                }
            }else{
                btnKomplain.setVisibility(View.GONE);
            }
            if(model.getTglAntar()!=null) {
                tglAntar = df.parse(model.getTglAntar());
                if(model.getCountUlasan().intValue() > 0){
                    btnUlasan.setVisibility(View.GONE);
                }else {
                    btnUlasan.setVisibility(View.VISIBLE);
                }
            }
            if(model.getTglAmbil()!=null)
                tglAmbil = df.parse(model.getTglAmbil());
        }catch (Exception ex){}

        txtNoTrans.setText("No Transaksi: "+model.getNoTrans());
        if(model.getNoReferensi()!=null){
            if(model.getNoReferensi().trim().equals("")){
                txtRef.setVisibility(View.GONE);
            }else{
                txtRef.setText("No Referensi: "+model.getNoReferensi());
                txtRef.setVisibility(View.VISIBLE);
            }
        }else{
            txtRef.setVisibility(View.GONE);
        }
        txtTglTrans.setText("Tanggal Transaksi: "+sdf1.format(tglTrans));
        txtbarang.setText("Barang: "+model.getNamaTipeBarang());
        txtKeluhan.setText("Keluhan: "+model.getKeluhan());
        txtAlamat.setText("Alamat: "+ model.getAlamat());

        String status = "";
        if(model.getKodeUserTeknisi()!=null && model.getTglAmbil()!=null &&
                model.getTglSelesai()!=null && model.getTglAntar()!=null)
            status = "Proses Selesai";
        else if(model.getKodeUserTeknisi()!=null && model.getTglAmbil()!=null &&
                model.getTglSelesai()!=null && model.getTglAntar()==null)
            status = "Teknisi akan menuju ke lokasi untuk mengantar barang";
        else if(model.getKodeUserTeknisi()!=null && model.getTglAmbil()!=null &&
                model.getTglSelesai()==null)
            status = "Proses pengerjaan teknisi";
        else if(model.getKodeUserTeknisi()!=null && model.getTglAmbil()==null &&
                model.getTglSelesai()==null)
            status = "Teknisi menuju lokasi untuk menjemput barang";
        else if(model.getKodeUserTeknisi()==null)
            status = "Menunggu Teknisi";
        else
            status = "Invalid Status";

        txtStatusProgress.setText("Status Transaksi: "+status);
        txtTeknisi.setText("Teknisi: "+model.getNamaUserTeknisi());
        txtTglAmbil.setText("Tanggal Pengambilan Barang: "+ (model.getTglAmbil()==null?"-":sdf1.format(tglAmbil)));
        txtPerikiraanBiaya.setText("Perkiraan Biaya: "+ (model.getBiayaPerkiraan()==null?"-":formatter.format(model.getBiayaPerkiraan().setScale(0))));
        txtPerkiraanHariSelesai.setText("Perkiraan Selesai: "+(model.getHariKerjaPerkiraan()==null?"-":(String.valueOf(model.getHariKerjaPerkiraan())))+" hari");
        txtCatatan.setText("Catatan Teknisi: "+ (model.getKetTeknisi()==null?"-":model.getKetTeknisi()));
        txtBiaya.setText("Biaya: "+ (model.getBiaya()==null?"-":("Rp "+formatter.format(model.getBiaya().setScale(0)))));
        txtNilaiPromo.setText("Potongan Promo: "+ (model.getNilaiPromo()==null?"0":("Rp "+formatter.format(model.getNilaiPromo().setScale(0)))));
        txtNetto.setText("Netto: "+ (model.getNetto()==null?"-":("Rp "+formatter.format(model.getNetto().setScale(0)))));
        txtTglSelesai.setText("Tanggal Selesai: "+ (model.getTglSelesai()==null?"-":sdf1.format(tglSelesai)));
        txtTglAntar.setText("Tanggal Pengantaran: "+ (model.getTglAntar()==null?"-":sdf1.format(tglAntar)));
        txtTglGaransi.setText("Garansi s/d: "+ (model.getTglBatasGaransi()==null?"-":sdf1.format(tglGaransi)));

        if(model.getNoReferensi()!=null){
            btnKomplain.setVisibility(View.GONE);
            btnUlasan.setVisibility(View.GONE);
            btnProfile.setVisibility(View.GONE);
            btnChat.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 8) {
            if(resultCode == RESULT_OK) {
                btnKomplain.setVisibility(View.GONE);
                btnUlasan.setVisibility(View.GONE);
                btnProfile.setVisibility(View.GONE);
                btnChat.setVisibility(View.GONE);
            }
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;
        LatLng latLng = new LatLng(model.getLatt(), model.getLongt());
        mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                .title("Titik Penjemputan")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        //mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,200));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
        //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel), 200, null);


        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        // Showing / hiding your current location
        //mGoogleMap.setMyLocationEnabled(true);
        // Enable / Disable zooming controls
        // Enable / Disable my location button
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        // Enable / Disable Compass icon
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        // Enable / Disable Rotate gesture
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
        //Enable / Disable Button Zooming
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(13), 200, null);
        createLocationRequest();
    }

    private void showDialogRate() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNeutralButtonText("Later")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setDefaultRating(3)
                .setTitle("Rate this Place")
                .setDescription("Please select some stars and give your feedback")
                .setDefaultComment("Masukkan Ulasan Anda!")
                .setStarColor(R.color.md_cyan_a400)
                .setNoteDescriptionTextColor(R.color.md_black)
                .setTitleTextColor(R.color.md_black)
                .setDescriptionTextColor(R.color.md_black)
                .setHint("Masukkan Ulasan")
                .setHintTextColor(R.color.md_grey_500)
                .setCommentTextColor(R.color.md_white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .create(TransListActivity.this)
                .show();
    }

    @Override
    public void onNegativeButtonClicked() {

    }

    @Override
    public void onNeutralButtonClicked() {

    }

    @Override
    public void onPositiveButtonClicked(int rate, String comment) {
        sendFeedback(model, userId, comment, rate);
    }

    private void sendFeedback(final TrHistCustModel entity, final String user, final String isi, final Integer rate){
        pDialog.setMessage("Please Wait ...");
        showDialog();
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        String tanggalNow = df.format(today);
        mApiService.saveRating(entity.getNoTrans().trim(), entity.getKodeUserTeknisi(), user, isi, rate,
                Utils.getDeviceName(), tanggalNow).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("value").equals("false")){
                            /*rating = jsonRESULTS.getJSONObject("rate").getInt("total_nilai");
                            countUser = jsonRESULTS.getJSONObject("rate").getInt("user_count");
                            reloadDataRating();*/
                            Toast.makeText(TransListActivity.this, "Terima kasih sudah berpartisipasi memberikan ulasan", Toast.LENGTH_LONG).show();
                            btnUlasan.setVisibility(View.GONE);
                        } else {
                            String error_message = jsonRESULTS.getString("message");
                            Toast.makeText(TransListActivity.this, error_message, Toast.LENGTH_LONG).show();
                        }
                        hideDialog();
                    }catch (JSONException e) {
                        hideDialog();
                        Toast.makeText(TransListActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }catch (IOException e) {
                        hideDialog();
                        Toast.makeText(TransListActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }else{
                    hideDialog();
                    Toast.makeText(TransListActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideDialog();
                Toast.makeText(TransListActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
            }
        });
    }
}
