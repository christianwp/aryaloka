package com.example.aryaloka.Transaksi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.aryaloka.Adapter.Transaksi.AdpTrHistTeknisi;
import com.example.aryaloka.Model.transaksi.TrHistoryTeknisiModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Utilities.AppController;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

public class TransHistTeknisiActivity extends AppCompatActivity {

    private ImageView imgBack;
    private AdpTrHistTeknisi adapter;
    private ListView lsvupload;
    private ArrayList<TrHistoryTeknisiModel> columnlist= new ArrayList<TrHistoryTeknisiModel>();
    private TextView tvstatus;
    private ProgressBar prbstatus;
    private String getUpload	="hist_teknisi_list.php";
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId;
    private int RESULT_DEPT = 9;
    private TrHistoryTeknisiModel model;
    private RadioGroup rg;
    private RadioButton rbSlsai, rbBlm, rbSemua;
    private Button btnProses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tr_history_teknisi);
        pref = new PrefUtil(this);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack		= (ImageView)findViewById(R.id.imgHistTeknisiBack);
        lsvupload	= (ListView)findViewById(R.id.listHistTeknisi);
        tvstatus	= (TextView)findViewById(R.id.txtListHistTeknisiStatus);
        prbstatus	= (ProgressBar)findViewById(R.id.prbListHistTeknisiStatus);
        rg = (RadioGroup)findViewById(R.id.rgListHistTeknisi);
        rbBlm = (RadioButton) findViewById(R.id.rbListHistTeknisiBlmSelesai);
        rbSlsai = (RadioButton) findViewById(R.id.rbListHistTeknisiSelesai);
        rbSemua = (RadioButton)findViewById(R.id.rbListHistTeknisiSemua);
        btnProses = (Button) findViewById(R.id.btnProsesListHistTeknisi);;

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status = rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()))==0?"B":rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()))==1?"S":"A";
                getDataUpload(Link.BASE_URL_API+getUpload, userId, status, level);
            }
        });

        adapter		= new AdpTrHistTeknisi(TransHistTeknisiActivity.this, R.layout.adp_tr_hist_teknisi, columnlist, level, userId);
        lsvupload.setAdapter(adapter);

        lsvupload.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                model = columnlist.get(position);
                Intent i = new Intent(getApplicationContext(), TransHistTeknisiItemActivity.class);
                i.putExtra("model", model);
                startActivityForResult(i, RESULT_DEPT);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    private void getDataUpload(String Url, final String kodeTeknisi, String status, String level){
        tvstatus.setVisibility(View.GONE);
        prbstatus.setVisibility(View.VISIBLE);
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

                    @Override
                    public void onResponse(String resp) {
                        try {
                            JSONObject response = new JSONObject(resp);
                            int sucses= response.getInt("success");
                            if (sucses==1){
                                tvstatus.setVisibility(View.GONE);
                                prbstatus.setVisibility(View.GONE);
                                adapter.clear();
                                JSONArray JsonArray = response.getJSONArray("uploade");
                                for (int i = 0; i < JsonArray.length(); i++) {
                                    JSONObject object = JsonArray.getJSONObject(i);
                                    TrHistoryTeknisiModel colums 	= new TrHistoryTeknisiModel();
                                    colums.setNoTrans(object.getString("c_notransaksi"));
                                    colums.setTglTrans(object.getString("dt_tgltransaksi"));
                                    colums.setKodeUserCust(object.getString("c_userid_cust"));
                                    colums.setNamaUserCust(object.getString("vc_username_cust")==null ? null :
                                            object.getString("vc_username_cust").equals("null")?null : object.getString("vc_username_cust"));
                                    colums.setKodePromo(object.getString("c_kodepromo")==null ? null :
                                            object.getString("c_kodepromo").equals("null")?null : object.getString("c_kodepromo"));
                                    colums.setNamaPromo(object.getString("vc_namapromo")==null ? null :
                                            object.getString("vc_namapromo").equals("null")?null : object.getString("vc_namapromo"));
                                    colums.setKodeTipeBarang(object.getString("c_tipe_barang")==null ? null :
                                            object.getString("c_tipe_barang").equals("null")?null : object.getString("c_tipe_barang"));
                                    colums.setNamaTipeBarang(object.getString("vc_namabarang")==null ? null :
                                            object.getString("vc_namabarang").equals("null")?null : object.getString("vc_namabarang"));
                                    colums.setKeluhan(object.getString("t_keluhan")==null ? null :
                                            object.getString("t_keluhan").equals("null")?null : object.getString("t_keluhan"));
                                    colums.setLatt(object.getDouble("d_latt"));
                                    colums.setLongt(object.getDouble("d_longt"));
                                    colums.setAlamat(object.getString("vc_alamat")==null ? null :
                                            object.getString("vc_alamat").equals("null")?null : object.getString("vc_alamat"));
                                    colums.setTglAmbil(object.getString("dt_tglpengambilan")==null ? null :
                                            object.getString("dt_tglpengambilan").equals("null")?null : object.getString("dt_tglpengambilan"));
                                    BigDecimal biayaPerkiraan = object.getString("n_biaya_perkiraan")==null ? null :
                                            object.getString("n_biaya_perkiraan").equals("null")?null : new BigDecimal(object.getDouble("n_biaya_perkiraan"));
                                    colums.setBiayaPerkiraan(biayaPerkiraan);
                                    BigDecimal nilaiPromo = object.getString("n_nilaipromo")==null ? null :
                                            object.getString("n_nilaipromo").equals("null")?null : new BigDecimal(object.getDouble("n_nilaipromo"));
                                    colums.setNilaiPromo(nilaiPromo);
                                    Integer hariPerkiraan = object.getString("i_hari_perkiraan")==null ? null :
                                            object.getString("i_hari_perkiraan").equals("null")?null : object.getInt("i_hari_perkiraan");
                                    colums.setHariKerjaPerkiraan(hariPerkiraan);
                                    colums.setKetTeknisi(object.getString("t_ket_teknisi")==null ? null :
                                            object.getString("t_ket_teknisi").equals("null")?null : object.getString("t_ket_teknisi"));
                                    colums.setTglSelesai(object.getString("dt_tglselesai")==null ? null :
                                            object.getString("dt_tglselesai").equals("null")?null : object.getString("dt_tglselesai"));
                                    BigDecimal biayaFinal = object.getString("n_biaya_final")==null ? null :
                                            object.getString("n_biaya_final").equals("null")?null : new BigDecimal(object.getDouble("n_biaya_final"));
                                    colums.setBiaya(biayaFinal);
                                    BigDecimal netto = object.getString("n_netto")==null ? null :
                                            object.getString("n_netto").equals("null")?null : new BigDecimal(object.getDouble("n_netto"));
                                    colums.setNetto(netto);
                                    colums.setTglAntar(object.getString("dt_tglpengantaran")==null ? null :
                                            object.getString("dt_tglpengantaran").equals("null")?null : object.getString("dt_tglpengantaran"));
                                    Integer hariGaransi = object.getString("i_hari_garansi")==null ? null :
                                            object.getString("i_hari_garansi").equals("null")?null : object.getInt("i_hari_garansi");
                                    colums.setHariGaransi(hariGaransi);
                                    colums.setTglBatasGaransi(object.getString("dt_batas_garansi")==null ? null :
                                            object.getString("dt_batas_garansi").equals("null")?null : object.getString("dt_batas_garansi"));
                                    colums.setCdstatus(object.getString("c_dstatus")==null ? null :
                                            object.getString("c_dstatus").equals("null")?null : object.getString("c_dstatus"));
                                    colums.setNoReferensi(object.getString("c_referensi")==null ? null :
                                            object.getString("c_referensi").equals("null")?null : object.getString("c_referensi"));
                                    columnlist.add(colums);
                                }
                            }else{
                                columnlist = new ArrayList<TrHistoryTeknisiModel>();
                                tvstatus.setVisibility(View.VISIBLE);
                                tvstatus.setText("Tidak Ada Data");
                                prbstatus.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter.notifyDataSetChanged();
                        adapter = new AdpTrHistTeknisi(TransHistTeknisiActivity.this, R.layout.adp_tr_hist_teknisi, columnlist, level, userId);
                        lsvupload.setAdapter(adapter);
                        //lsvupload.invalidate();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check Koneksi Internet Anda");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof AuthFailureError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("AuthFailureError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ServerError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ServerError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof NetworkError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check NetworkError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ParseError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ParseError");
                    prbstatus.setVisibility(View.GONE);
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                if(level.equals("A")){
                    params.put("kodeTeknisi", "%");
                }else {
                    params.put("kodeTeknisi", kodeTeknisi);
                }
                params.put("status", status);
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
                return params;
            }
        };
        AppController.getInstance().getRequestQueue().getCache().invalidate(Url, true);
        AppController.getInstance().addToRequestQueue(register);
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        if(tekan) {
            columnlist = new ArrayList<TrHistoryTeknisiModel>();
            adapter = new AdpTrHistTeknisi(TransHistTeknisiActivity.this, R.layout.adp_tr_hist_teknisi, columnlist, level, userId);
            lsvupload.setAdapter(adapter);
            String status = rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId())) == 0 ? "B" : rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId())) == 1 ? "S" : "A";
            getDataUpload(Link.BASE_URL_API + getUpload, userId, status, level);
        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_DEPT) {
            if(resultCode == RESULT_OK) {
                columnlist = new ArrayList<TrHistoryTeknisiModel>();
                adapter		= new AdpTrHistTeknisi(TransHistTeknisiActivity.this, R.layout.adp_tr_hist_teknisi, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                String status = rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()))==0?"B":rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId()))==1?"S":"A";
                getDataUpload(Link.BASE_URL_API+getUpload, userId, status, level);
            }
        }
    }
}