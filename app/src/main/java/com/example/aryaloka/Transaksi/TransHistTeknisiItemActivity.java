package com.example.aryaloka.Transaksi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aryaloka.Model.transaksi.TrHistCustModel;
import com.example.aryaloka.Model.transaksi.TrHistoryTeknisiModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.example.aryaloka.Utilities.Utils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.aryaloka.Utilities.Utils.getDecimalFormattedString;
import static com.example.aryaloka.Utilities.Utils.trimCommaOfString;

public class TransHistTeknisiItemActivity extends AppCompatActivity  implements OnMapReadyCallback {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level;
    private ImageView imgBack;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private TextView txtNoTrans, txtTglTrans, txtbarang, txtKeluhan, txtStatusProgress, txtPerikiraanBiaya, txtTglAmbil,
            txtPerkiraanHariSelesai, txtCatatan, txtBiaya, txtNilaiPromo, txtNetto, txtTglSelesai, txtTglAntar, txtTglGaransi, txtAlamat;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private static final long INTERVAL = 1000 * 2 * 1; //2 detik
    private static final long FASTEST_INTERVAL = 1000 * 1 * 1; // 1 detik
    private GoogleMap mGoogleMap;
    private float zoomLevel = (float) 16.0;
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
    private DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
    private Button btnChat, btnProses;
    private Date now = Calendar.getInstance().getTime();
    private TrHistoryTeknisiModel model;
    private TextInputLayout ilNote1, ilEstBiaya1, ilEstHari1;
    private TextInputEditText edNote1, edEstBiaya1, edEstHari1;
    private TextInputLayout ilNote2, ilEstBiaya2, ilPromo2, ilNetto2;
    private TextInputEditText edNote2, edEstBiaya2, edPromo2, edNetto2;
    private TextInputLayout ilNote3, ilGaransi3;
    private TextInputEditText edNote3, edGaransi3;
    private Button btnSave1, btnSave2, btnSave3;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trans_item_view_teknisi_activity);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}
        Intent i = getIntent();
        if (i != null){
            try {
                model = (TrHistoryTeknisiModel) i.getSerializableExtra("model");
            }catch (Exception e) {}
        }

        imgBack = (ImageView)findViewById(R.id.ImgBackInfoTransaksiKary);
        txtNoTrans = (TextView)findViewById(R.id.txtNomorTrInfoTransaksiKary);
        txtTglTrans = (TextView)findViewById(R.id.txtTglTrInfoTransaksiKary);
        txtbarang = (TextView)findViewById(R.id.txtBarangInfoTransaksiKary);
        txtKeluhan = (TextView)findViewById(R.id.txtKeluhanInfoTransaksiKary);
        txtStatusProgress = (TextView)findViewById(R.id.txtStatusProgressInfoTransaksiKary);
        txtPerikiraanBiaya = (TextView)findViewById(R.id.txtPerkiraanBiayaInfoTransaksiKary);
        txtPerkiraanHariSelesai = (TextView)findViewById(R.id.txtPerkiraanHariSelesaiInfoTransaksiKary);
        txtCatatan = (TextView)findViewById(R.id.txtCatatanInfoTransaksiKary);
        txtBiaya = (TextView)findViewById(R.id.txtBiayaFinalInfoTransaksiKary);
        txtNilaiPromo = (TextView)findViewById(R.id.txtNilaiPromoInfoTransaksiKary);
        txtNetto = (TextView)findViewById(R.id.txtNettoInfoTransaksiKary);
        txtTglSelesai = (TextView)findViewById(R.id.txtTglSelesaiInfoTransaksiKary);
        txtTglAmbil = (TextView)findViewById(R.id.txtTglAmbilInfoTransaksiKary);
        txtTglAntar = (TextView)findViewById(R.id.txtTglPengantaranInfoTransaksiKary);
        txtTglGaransi = (TextView)findViewById(R.id.txtTglGaransiInfoTransaksiKary);
        txtAlamat = (TextView)findViewById(R.id.txtAlamatInfoTransaksiKary);
        btnChat = (Button)findViewById(R.id.btnChatTeknisiInfoTransaksKary);
        btnProses = (Button)findViewById(R.id.btnProsesInfoTransaksiKary);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.frgMapsInfoTransaksiKary);
        fm.getMapAsync(this);

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TransHistTeknisiItemActivity.this, TransChatActivity.class);
                i.putExtra("noTrans", model.getNoTrans());
                i.putExtra("idUser", model.getKodeUserCust());
                i.putExtra("idTeknisi", userId);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getTglAmbil()!=null && model.getTglSelesai()!=null && model.getTglAntar()==null) {
                    //isi dt_tglpengantaran, i_hari_garansi, dt_batas_garansi. status = 3
                    View3 view3 = new View3(TransHistTeknisiItemActivity.this);
                    view3.showAlert(model);
                }else if(model.getTglAmbil()!=null && model.getTglSelesai()==null) {
                    //isi dt_tglselesai, n_biaya_final, n_netto. status = 2
                    View2 view2 = new View2(TransHistTeknisiItemActivity.this);
                    view2.showAlert(model);
                }else if(model.getTglAmbil()==null && model.getTglSelesai()==null) {
                    //isi dt_tglpengambilan, n_biaya_perkiraan, i_hari_perkiraan, t_let_teknisi. status = 1
                    View1 view1 = new View1(TransHistTeknisiItemActivity.this);
                    view1.showAlert(model);
                }else{
                    Toast.makeText(TransHistTeknisiItemActivity.this, "INVALID MODE", Toast.LENGTH_LONG).show();
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        modelToView(model);
    }

    private void modelToView(TrHistoryTeknisiModel modelku){
        Date tglTrans = Calendar.getInstance().getTime();
        Date tglAntar = Calendar.getInstance().getTime();
        Date tglSelesai = Calendar.getInstance().getTime();
        Date tglGaransi = Calendar.getInstance().getTime();
        Date tglAmbil = Calendar.getInstance().getTime();
        try{
            tglTrans = df.parse(modelku.getTglTrans());
            if(modelku.getTglSelesai()!=null)
                tglSelesai = df.parse(modelku.getTglSelesai());
            if(modelku.getTglBatasGaransi()!=null)
                tglGaransi = df.parse(modelku.getTglBatasGaransi());
            if(modelku.getTglAntar()!=null)
                tglAntar = df.parse(modelku.getTglAntar());
            if(modelku.getTglAmbil()!=null)
                tglAmbil = df.parse(modelku.getTglAmbil());
        }catch (Exception ex){}

        txtNoTrans.setText("No Transaksi: "+modelku.getNoTrans());
        txtTglTrans.setText("Tanggal Transaksi: "+sdf1.format(tglTrans));
        txtbarang.setText("Barang: "+modelku.getNamaTipeBarang());
        txtKeluhan.setText("Keluhan: "+modelku.getKeluhan());
        txtAlamat.setText("Alamat: "+ modelku.getAlamat());

        String status = "";
        if(modelku.getTglAmbil()!=null && modelku.getTglSelesai()!=null && modelku.getTglAntar()!=null) {
            status = "Proses Selesai";
            btnProses.setVisibility(View.GONE);
            btnChat.setVisibility(View.GONE);
        }else if(modelku.getTglAmbil()!=null && modelku.getTglSelesai()!=null && modelku.getTglAntar()==null) {
            status = "Teknisi akan menuju ke lokasi untuk mengantar barang";
            btnProses.setVisibility(View.VISIBLE);
            btnChat.setVisibility(View.VISIBLE);
        }else if(modelku.getTglAmbil()!=null && modelku.getTglSelesai()==null) {
            status = "Proses pengerjaan teknisi";
            btnProses.setVisibility(View.VISIBLE);
            btnChat.setVisibility(View.VISIBLE);
        }else if(modelku.getTglAmbil()==null && modelku.getTglSelesai()==null) {
            status = "Teknisi menuju lokasi untuk menjemput barang";
            btnProses.setVisibility(View.VISIBLE);
            btnChat.setVisibility(View.VISIBLE);
        }else {
            status = "Invalid Status";
            btnProses.setVisibility(View.GONE);
            btnChat.setVisibility(View.GONE);
        }

        txtStatusProgress.setText("Status Transaksi: "+status);
        txtTglAmbil.setText("Tanggal Pengambilan Barang: "+ (modelku.getTglAmbil()==null?"-":sdf1.format(tglAmbil)));
        txtPerikiraanBiaya.setText("Perkiraan Biaya: "+ (modelku.getBiayaPerkiraan()==null?"-":formatter.format(modelku.getBiayaPerkiraan().setScale(0))));
        txtPerkiraanHariSelesai.setText("Perkiraan Selesai: "+(modelku.getHariKerjaPerkiraan()==null?"-":(String.valueOf(modelku.getHariKerjaPerkiraan())))+" hari");
        txtCatatan.setText("Catatan Teknisi: "+ (modelku.getKetTeknisi()==null?"-":modelku.getKetTeknisi()));
        txtBiaya.setText("Biaya: "+ (modelku.getBiaya()==null?"-":("Rp "+formatter.format(modelku.getBiaya().setScale(0)))));
        txtNilaiPromo.setText("Potongan Promo: "+ (modelku.getNilaiPromo()==null?"0":("Rp "+formatter.format(modelku.getNilaiPromo().setScale(0)))));
        txtNetto.setText("Netto: "+ (modelku.getNetto()==null?"-":("Rp "+formatter.format(modelku.getNetto().setScale(0)))));
        txtTglSelesai.setText("Tanggal Selesai: "+ (modelku.getTglSelesai()==null?"-":sdf1.format(tglSelesai)));
        txtTglAntar.setText("Tanggal Pengantaran: "+ (modelku.getTglAntar()==null?"-":sdf1.format(tglAntar)));
        txtTglGaransi.setText("Garansi s/d: "+ (modelku.getTglBatasGaransi()==null?"-":sdf1.format(tglGaransi)));
    }

    public class View1 {
        Context context;
        public View1(Context context) {
            this.context = context;
        }

        public void showAlert(TrHistoryTeknisiModel modelku){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View view = inflater.inflate(R.layout.tr_info1, null);
            ilNote1 = (TextInputLayout) view.findViewById(R.id.input_layout_info1_ket);
            edNote1 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo1Ket);
            ilEstBiaya1 = (TextInputLayout) view.findViewById(R.id.input_layout_info1_estbiaya);
            edEstBiaya1 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo1Estbiaya);
            ilEstHari1 = (TextInputLayout) view.findViewById(R.id.input_layout_info1_esthari);
            edEstHari1 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo1EstHari);
            btnSave1 = (Button) view.findViewById(R.id.btnSimpanInfo1);

            alertDialog.setView(view);
            final AlertDialog show = alertDialog.show();

            edEstHari1.setText("0");
            edEstBiaya1.setText("0");

            edEstBiaya1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(final Editable s) {
                    try{
                        edEstBiaya1.removeTextChangedListener(this);
                        String value = edEstBiaya1.getText().toString();
                        if (value != null && !value.equals("")){
                            if(value.startsWith(".")){
                                edEstBiaya1.setText("0.");
                            }
                            String str = edEstBiaya1.getText().toString().replaceAll(",", "");
                            Integer val = Integer.parseInt(str);
                            if (!value.equals(""))
                                edEstBiaya1.setText(getDecimalFormattedString(val.toString()));
                            edEstBiaya1.setSelection(edEstBiaya1.getText().toString().length());
                        }
                        edEstBiaya1.addTextChangedListener(this);
                        return;
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                        edEstBiaya1.addTextChangedListener(this);
                    }
                }
            });

            edEstHari1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(final Editable s) {
                    try{
                        edEstHari1.removeTextChangedListener(this);
                        String value = edEstHari1.getText().toString();
                        if (value != null && !value.equals("")){
                            if(value.startsWith(".")){
                                edEstHari1.setText("0.");
                            }
                            String str = edEstHari1.getText().toString().replaceAll(",", "");
                            Integer val = Integer.parseInt(str);
                            if (!value.equals(""))
                                edEstHari1.setText(getDecimalFormattedString(val.toString()));
                            edEstHari1.setSelection(edEstHari1.getText().toString().length());
                        }
                        edEstHari1.addTextChangedListener(this);
                        return;
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                        edEstHari1.addTextChangedListener(this);
                    }
                }
            });

            btnSave1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateHari() && validateBiaya() ){
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Data akan diproses?")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        simpan1(modelku.getNoTrans(), "1", (new BigDecimal(trimCommaOfString(edEstBiaya1.getText().toString()))),
                                                Integer.valueOf(edEstHari1.getText().toString()), edNote1.getText().toString(), userId);
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        android.app.AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                }
            });
        }

        private boolean validateHari() {
            boolean value;
            if (edEstHari1.getText().toString().isEmpty()){
                value=false;
                requestFocus(edEstHari1);
                Toast.makeText(context, "Estimasi Hari belum diisi", Toast.LENGTH_LONG).show();
            } else if (edEstHari1.getText().toString().equals("0")){
                value=false;
                requestFocus(edEstHari1);
                Toast.makeText(context, "Estimasi Hari belum diisi", Toast.LENGTH_LONG).show();
            } else {
                value=true;
            }
            return value;
        }

        private boolean validateBiaya() {
            boolean value;
            if (edEstBiaya1.getText().toString().isEmpty()){
                value=false;
                requestFocus(edEstBiaya1);
                Toast.makeText(context, "Estimasi biaya belum diisi", Toast.LENGTH_LONG).show();
            } else if (edEstBiaya1.getText().toString().equals("0")){
                value=false;
                requestFocus(edEstBiaya1);
                Toast.makeText(context, "Estimasi Biaya belum diisi", Toast.LENGTH_LONG).show();
            } else {
                value=true;
            }
            return value;
        }

        private void requestFocus(View view) {
            if (view.requestFocus()) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        }
    }

    public class View2 {
        Context context;
        public View2(Context context) {
            this.context = context;
        }

        public void showAlert(TrHistoryTeknisiModel modelku){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View view = inflater.inflate(R.layout.tr_info2, null);
            ilNote2 = (TextInputLayout) view.findViewById(R.id.input_layout_info2_ket);
            edNote2 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo2Ket);
            ilEstBiaya2 = (TextInputLayout) view.findViewById(R.id.input_layout_info2_estbiaya);
            edEstBiaya2 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo2Estbiaya);
            ilPromo2 = (TextInputLayout) view.findViewById(R.id.input_layout_info2_nilaipromo);
            edPromo2 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo2Nilaipromo);
            ilNetto2 = (TextInputLayout) view.findViewById(R.id.input_layout_info2_netto);
            edNetto2 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo2Netto);
            btnSave2 = (Button) view.findViewById(R.id.btnSimpanInfo2);

            alertDialog.setView(view);
            final AlertDialog show = alertDialog.show();

            edPromo2.setText(String.valueOf(formatter.format(modelku.getNilaiPromo().setScale(0))));
            edEstBiaya2.setText("0");
            edNetto2.setText("0");

            edEstBiaya2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(final Editable s) {
                    try{
                        edEstBiaya2.removeTextChangedListener(this);
                        String value = edEstBiaya2.getText().toString();
                        if (value != null && !value.equals("")){
                            if(value.startsWith(".")){
                                edEstBiaya2.setText("0.");
                            }
                            String str = edEstBiaya2.getText().toString().replaceAll(",", "");
                            Integer val = Integer.parseInt(str);
                            if (!value.equals(""))
                                edEstBiaya2.setText(getDecimalFormattedString(val.toString()));
                            edEstBiaya2.setSelection(edEstBiaya2.getText().toString().length());
                            BigDecimal netto = new BigDecimal(val).subtract(modelku.getNilaiPromo());
                            edNetto2.setText(formatter.format(netto.setScale(0)));
                        }
                        edEstBiaya2.addTextChangedListener(this);
                        return;
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                        edEstBiaya2.addTextChangedListener(this);
                    }
                }
            });

            btnSave2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateBiaya() ){
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Data akan diproses?")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        simpan2(modelku.getNoTrans(), "2", (new BigDecimal(trimCommaOfString(edEstBiaya2.getText().toString()))),
                                                edNote2.getText().toString(), userId);
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        android.app.AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }


                }
            });
        }

        private boolean validateBiaya() {
            boolean value;
            if (edEstBiaya2.getText().toString().isEmpty()){
                value=false;
                requestFocus(edEstBiaya2);
                Toast.makeText(context, "Biaya belum diisi", Toast.LENGTH_LONG).show();
            } else if (edEstBiaya2.getText().toString().equals("0")){
                value=false;
                requestFocus(edEstBiaya2);
                Toast.makeText(context, "Biaya belum diisi", Toast.LENGTH_LONG).show();
            } else {
                value=true;
            }
            return value;
        }

        private void requestFocus(View view) {
            if (view.requestFocus()) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        }
    }

    public class View3 {
        Context context;
        public View3(Context context) {
            this.context = context;
        }

        public void showAlert(TrHistoryTeknisiModel modelku){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View view = inflater.inflate(R.layout.tr_info3, null);
            ilNote3 = (TextInputLayout) view.findViewById(R.id.input_layout_info3_ket);
            edNote3 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo3Ket);
            ilGaransi3 = (TextInputLayout) view.findViewById(R.id.input_layout_info3_harigaransi);
            edGaransi3 = (TextInputEditText) view.findViewById(R.id.eAddViewFormInfo3Harigaransi);
            btnSave3 = (Button) view.findViewById(R.id.btnSimpanInfo3);

            alertDialog.setView(view);
            final AlertDialog show = alertDialog.show();

            edGaransi3.setText("0");

            edGaransi3.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(final Editable s) {
                    try{
                        edGaransi3.removeTextChangedListener(this);
                        String value = edGaransi3.getText().toString();
                        if (value != null && !value.equals("")){
                            if(value.startsWith(".")){
                                edGaransi3.setText("0.");
                            }
                            String str = edGaransi3.getText().toString().replaceAll(",", "");
                            Integer val = Integer.parseInt(str);
                            if (!value.equals(""))
                                edGaransi3.setText(getDecimalFormattedString(val.toString()));
                            edGaransi3.setSelection(edEstHari1.getText().toString().length());
                        }
                        edGaransi3.addTextChangedListener(this);
                        return;
                    }
                    catch (Exception ex){
                        ex.printStackTrace();
                        edGaransi3.addTextChangedListener(this);
                    }
                }
            });

            btnSave3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateGaransi()){
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Data akan diproses?")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Calendar cal = Calendar.getInstance();
                                        cal.add(Calendar.DATE, Integer.valueOf(edGaransi3.getText().toString()));
                                        Date tglGransi = Utils.getLastTimeOfDay(cal.getTime());
                                        simpan3(modelku.getNoTrans(), "3", df.format(tglGransi),
                                                Integer.valueOf(edGaransi3.getText().toString()), edNote3.getText().toString(), userId);
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        android.app.AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }


                }
            });
        }

        private boolean validateGaransi() {
            boolean value;
            if (edGaransi3.getText().toString().isEmpty()){
                value=false;
                requestFocus(edGaransi3);
                Toast.makeText(context, "Garansi belum diisi", Toast.LENGTH_LONG).show();
            } else if (edGaransi3.getText().toString().equals("0")){
                value=false;
                requestFocus(edGaransi3);
                Toast.makeText(context, "Garansi belum diisi", Toast.LENGTH_LONG).show();
            } else {
                value=true;
            }
            return value;
        }

        private void requestFocus(View view) {
            if (view.requestFocus()) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        }
    }

    private void simpan1(final String noTrans, final String status, final BigDecimal estBiaya,
                         final Integer estHari, final String ketTeksnisi, final String userId){
        pDialog.setMessage("Proses Simpan ...");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow = df.format(today);
        mApiService.updateServis1(noTrans, status, ketTeksnisi, estBiaya, estHari, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    Toast.makeText(TransHistTeknisiItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    model.setBiayaPerkiraan(estBiaya);
                                    model.setHariKerjaPerkiraan(estHari);
                                    model.setKetTeknisi(ketTeksnisi);
                                    model.setTglAmbil(tanggalNow);
                                    hideDialog();
                                    modelToView(model);

                                    Utils.sendNotification(mApiService, "Aryaloka App", "Barang anda sedang dalam proses pengerjaan oleh teknisi kami", model.getKodeUserCust());

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                }else{
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(TransHistTeknisiItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            }catch (JSONException e) {
                                hideDialog();
                                Toast.makeText(TransHistTeknisiItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }catch (IOException e) {
                                hideDialog();
                                Toast.makeText(TransHistTeknisiItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }else{
                            Toast.makeText(TransHistTeknisiItemActivity.this, "Gagal update data", Toast.LENGTH_LONG).show();
                            hideDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(TransHistTeknisiItemActivity.this, "Gagal update data", Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                });
    }

    private void simpan2(final String noTrans, final String status,
                         final BigDecimal biaya, final String ketTeksnisi, final String userId){
        pDialog.setMessage("Proses Simpan ...");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow = df.format(today);
        mApiService.updateServis2(noTrans, status, ketTeksnisi, biaya, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    Toast.makeText(TransHistTeknisiItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    model.setBiaya(biaya);
                                    model.setNetto(biaya.subtract(model.getNilaiPromo()));
                                    model.setKetTeknisi(ketTeksnisi);
                                    model.setTglSelesai(tanggalNow);
                                    hideDialog();
                                    modelToView(model);

                                    Utils.sendNotification(mApiService, "Aryaloka App", "Proses pengerjaan sudah selesai. Teknisi kami akan menuju ke lokasi pengantaran", model.getKodeUserCust());

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                }else{
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(TransHistTeknisiItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            }catch (JSONException e) {
                                hideDialog();
                                Toast.makeText(TransHistTeknisiItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }catch (IOException e) {
                                hideDialog();
                                Toast.makeText(TransHistTeknisiItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }else{
                            Toast.makeText(TransHistTeknisiItemActivity.this, "Gagal update data", Toast.LENGTH_LONG).show();
                            hideDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(TransHistTeknisiItemActivity.this, "Gagal update data", Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                });
    }

    private void simpan3(final String noTrans, final String status, final String tglGaransi,
                         final Integer harigaransi, final String ketTeksnisi, final String userId){
        pDialog.setMessage("Proses Simpan ...");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow = df.format(today);
        mApiService.updateServis3(noTrans, status, ketTeksnisi, harigaransi, tglGaransi, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    Toast.makeText(TransHistTeknisiItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    model.setHariGaransi(harigaransi);
                                    model.setTglBatasGaransi(tglGaransi);
                                    model.setKetTeknisi(ketTeksnisi);
                                    model.setTglAntar(tanggalNow);
                                    hideDialog();
                                    modelToView(model);

                                    Utils.sendNotification(mApiService, "Aryaloka App", "Barang sudah sampai tujuan. Cek berkala garansi jika ada kendala.", model.getKodeUserCust());

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                }else{
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(TransHistTeknisiItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            }catch (JSONException e) {
                                hideDialog();
                                Toast.makeText(TransHistTeknisiItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }catch (IOException e) {
                                hideDialog();
                                Toast.makeText(TransHistTeknisiItemActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }else{
                            Toast.makeText(TransHistTeknisiItemActivity.this, "Gagal update data", Toast.LENGTH_LONG).show();
                            hideDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(TransHistTeknisiItemActivity.this, "Gagal update data", Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = map;
        LatLng latLng = new LatLng(model.getLatt(), model.getLongt());
        mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                .title("Titik Pengambilan")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        //mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,200));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
        //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel), 200, null);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        // Showing / hiding your current location
        //mGoogleMap.setMyLocationEnabled(true);
        // Enable / Disable zooming controls
        // Enable / Disable my location button
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        // Enable / Disable Compass icon
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        // Enable / Disable Rotate gesture
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
        // Enable / Disable zooming functionality
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
        //Enable / Disable Button Zooming
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(13), 200, null);
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
