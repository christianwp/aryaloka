package com.example.aryaloka.Transaksi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.aryaloka.Model.transaksi.TrHistCustModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.example.aryaloka.Utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.aryaloka.Utilities.Utils.trimCommaOfString;

public class TransGaransiActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status;
    private ImageView imgBack;
    private TextInputEditText edRef, edKeluhan, edAlamat;
    private TextInputLayout ilRef, ilKeluhan, ilAlamat;
    private Button btnSave, btnMap;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private CheckBox ckAlamat;
    private LocationManager locationManager ;
    private boolean GpsStatus, alamatManual=false ;
    private double latitude, longtitude;
    private int RESULT_MAP = 5;
    private boolean alamatMaps;
    private TrHistCustModel model;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tr_form_garansi);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}
        Intent i = getIntent();
        if (i != null){
            try {
                model = (TrHistCustModel) i.getSerializableExtra("model");
            }catch (Exception e) {}
        }
        latitude = model.getLatt();
        longtitude = model.getLongt();
        imgBack = (ImageView)findViewById(R.id.imgViewFormGaransiBack);
        edRef = (TextInputEditText) findViewById(R.id.eAddViewFormGaransiNoRef);
        ilRef = (TextInputLayout) findViewById(R.id.input_layout_FormGaransi_noref);
        edKeluhan = (TextInputEditText) findViewById(R.id.eAddViewFormGaransiKeluhan);
        ilKeluhan = (TextInputLayout) findViewById(R.id.input_layout_FormGaransi_keluhan);
        edAlamat = (TextInputEditText) findViewById(R.id.eAddViewFormGaransiAlamat);
        ilAlamat = (TextInputLayout) findViewById(R.id.input_layout_FormGaransi_alamat);
        btnMap = (Button) findViewById(R.id.bInfoFormGaransiMap);
        ckAlamat = (CheckBox) findViewById(R.id.ckFormGaransiAlamatManual);
        btnSave = (Button) findViewById(R.id.btnViewFormGaransiSave);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateKeluhan() && validateAlamat() && validateLongt()){
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(TransGaransiActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    insertData(model.getNoTrans(), model.getKodeTipeBarang(), edKeluhan.getText().toString(),
                                            latitude, longtitude, edAlamat.getText().toString(), userId);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckGpsStatus() ;
                if(GpsStatus == true){
                    Intent i = new Intent(getApplication(), MapsActivity.class);
                    startActivityForResult(i, RESULT_MAP);
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TransGaransiActivity.this);
                    builder.setTitle("Perhatian");
                    builder.setMessage("GPS belum diaktifkan\nAktifkan dahulu!")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert);
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        edRef.setText(model.getNoTrans());
        edAlamat.setText(model.getAlamat());
    }

    private boolean validateLongt() {
        boolean value;
        Double latt = new Double(latitude);
        Double longt = new Double(longtitude);
        if (latt == null || longt == null){
            value=false;
            Toast.makeText(TransGaransiActivity.this, "Harap set lokasi map", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private void insertData(String noRef, String kodeBarng, String keluhan, double latt, double longt, String alamat, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        String tanggalNow = df.format(today);
        Integer year = cal.get(Calendar.YEAR);
        Integer month = cal.get(Calendar.MONTH)+1;
        mApiService.insertGaransi(kodeBarng, keluhan, noRef, latt, longt, alamat, tanggalNow, userId, year, month)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(TransGaransiActivity.this, "DATA BERHASIL DISIMPAN.", Toast.LENGTH_LONG).show();

                                    String idTeknisi = jsonRESULTS.getString("idTeknisi");
                                    Utils.sendNotification(mApiService, "Aryaloka App", "Ada Transaksi pengajuan proses garansi yang masuk", idTeknisi);
                                    Utils.sendNotification(mApiService, "Aryaloka App", "Teknisi akan segera ke lokasi anda. Harap menunggu!", userId);

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(TransGaransiActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(TransGaransiActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(TransGaransiActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(TransGaransiActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(TransGaransiActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean validateKeluhan() {
        boolean value;
        if (edKeluhan.getText().toString().isEmpty()){
            value=false;
            requestFocus(edKeluhan);
            ilKeluhan.setError(getString(R.string.err_msg_keluhan));
        } else if (edKeluhan.length() > ilKeluhan.getCounterMaxLength()) {
            value=false;
            ilKeluhan.setError("Max character length is " + ilKeluhan.getCounterMaxLength());
        } else {
            value=true;
            ilKeluhan.setError(null);
        }
        return value;
    }

    private boolean validateAlamat() {
        boolean value;
        if (edAlamat.getText().toString().isEmpty()){
            value=false;
            requestFocus(edAlamat);
            ilAlamat.setError(getString(R.string.err_msg_alamat));
        } else {
            value=true;
            ilAlamat.setError(null);
        }
        return value;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void CheckGpsStatus(){
        locationManager = (LocationManager)TransGaransiActivity.this.getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
