package com.example.aryaloka.Transaksi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.example.aryaloka.Adapter.Transaksi.AdpTrHistCust;
import com.example.aryaloka.Model.master.MasterKaryawanModel;
import com.example.aryaloka.Model.transaksi.TrHistCustModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Utilities.AppController;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

public class TransHistCustActivity extends AppCompatActivity {

    private ImageView ImgAdd, imgBack;
    private AdpTrHistCust adapter;
    private ListView lsvupload;
    private ArrayList<TrHistCustModel> columnlist= new ArrayList<TrHistCustModel>();
    private TextView tvstatus;
    private ProgressBar prbstatus;
    private String getUpload	="hist_cust_list.php";
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId;
    private int RESULT_DEPT = 9;
    private TrHistCustModel model;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tr_history_cust);
        pref = new PrefUtil(this);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}
        ImgAdd		= (ImageView)findViewById(R.id.imgListHistCustAdd);
        imgBack		= (ImageView)findViewById(R.id.imgHistCustBack);
        lsvupload	= (ListView)findViewById(R.id.listHistCust);
        tvstatus	= (TextView)findViewById(R.id.txtListHistCustStatus);
        prbstatus	= (ProgressBar)findViewById(R.id.prbListHistCustStatus);

        adapter		= new AdpTrHistCust(TransHistCustActivity.this, R.layout.adp_tr_hist_cust, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
        getDataUpload(Link.BASE_URL_API+getUpload, userId);

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(level.equals("A")){
                    Intent i  = new Intent(TransHistCustActivity.this, TransHistCustItemActivity.class);
                    startActivityForResult(i, 9);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        });

        lsvupload.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                model = columnlist.get(position);
                Intent i = new Intent(getApplicationContext(), TransListActivity.class);
                i.putExtra("model", model);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getDataUpload(String Url, final String kodeCust){
        tvstatus.setVisibility(View.GONE);
        prbstatus.setVisibility(View.VISIBLE);
        JsonObjectRequest jsonget = new JsonObjectRequest(Request.Method.GET, Url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int sucses= response.getInt("success");
                            if (sucses==1){
                                tvstatus.setVisibility(View.GONE);
                                prbstatus.setVisibility(View.GONE);
                                adapter.clear();
                                JSONArray JsonArray = response.getJSONArray("uploade");
                                for (int i = 0; i < JsonArray.length(); i++) {
                                    JSONObject object = JsonArray.getJSONObject(i);
                                    TrHistCustModel colums 	= new TrHistCustModel();
                                    colums.setNoTrans(object.getString("c_notransaksi"));
                                    colums.setTglTrans(object.getString("dt_tgltransaksi"));
                                    colums.setKodeUserCust(object.getString("c_userid_cust"));
                                    colums.setNamaUserCust(object.getString("vc_username_cust")==null ? null :
                                                    object.getString("vc_username_cust").equals("null")?null : object.getString("vc_username_cust"));
                                    colums.setKodePromo(object.getString("c_kodepromo")==null ? null :
                                            object.getString("c_kodepromo").equals("null")?null : object.getString("c_kodepromo"));
                                    colums.setNamaPromo(object.getString("vc_namapromo")==null ? null :
                                            object.getString("vc_namapromo").equals("null")?null : object.getString("vc_namapromo"));
                                    colums.setKodeTipeBarang(object.getString("c_tipe_barang")==null ? null :
                                            object.getString("c_tipe_barang").equals("null")?null : object.getString("c_tipe_barang"));
                                    colums.setNamaTipeBarang(object.getString("vc_namabarang")==null ? null :
                                            object.getString("vc_namabarang").equals("null")?null : object.getString("vc_namabarang"));
                                    colums.setKeluhan(object.getString("t_keluhan")==null ? null :
                                            object.getString("t_keluhan").equals("null")?null : object.getString("t_keluhan"));
                                    colums.setLatt(object.getDouble("d_latt"));
                                    colums.setLongt(object.getDouble("d_longt"));
                                    colums.setAlamat(object.getString("vc_alamat")==null ? null :
                                            object.getString("vc_alamat").equals("null")?null : object.getString("vc_alamat"));
                                    colums.setKodeUserTeknisi(object.getString("c_userid_teknisi")==null ? null :
                                            object.getString("c_userid_teknisi").equals("null")?null : object.getString("c_userid_teknisi"));
                                    colums.setNamaUserTeknisi(object.getString("vc_username_teknisi")==null ? null :
                                            object.getString("vc_username_teknisi").equals("null")?null : object.getString("vc_username_teknisi"));
                                    colums.setTglAmbil(object.getString("dt_tglpengambilan")==null ? null :
                                            object.getString("dt_tglpengambilan").equals("null")?null : object.getString("dt_tglpengambilan"));
                                    BigDecimal biayaPerkiraan = object.getString("n_biaya_perkiraan")==null ? null :
                                            object.getString("n_biaya_perkiraan").equals("null")?null : new BigDecimal(object.getDouble("n_biaya_perkiraan"));
                                    colums.setBiayaPerkiraan(biayaPerkiraan);
                                    BigDecimal nilaiPromo = object.getString("n_nilaipromo")==null ? null :
                                            object.getString("n_nilaipromo").equals("null")?null : new BigDecimal(object.getDouble("n_nilaipromo"));
                                    colums.setNilaiPromo(nilaiPromo);
                                    Integer hariPerkiraan = object.getString("i_hari_perkiraan")==null ? null :
                                            object.getString("i_hari_perkiraan").equals("null")?null : object.getInt("i_hari_perkiraan");
                                    colums.setHariKerjaPerkiraan(hariPerkiraan);
                                    colums.setKetTeknisi(object.getString("t_ket_teknisi")==null ? null :
                                            object.getString("t_ket_teknisi").equals("null")?null : object.getString("t_ket_teknisi"));
                                    colums.setTglSelesai(object.getString("dt_tglselesai")==null ? null :
                                            object.getString("dt_tglselesai").equals("null")?null : object.getString("dt_tglselesai"));
                                    BigDecimal biayaFinal = object.getString("n_biaya_final")==null ? null :
                                            object.getString("n_biaya_final").equals("null")?null : new BigDecimal(object.getDouble("n_biaya_final"));
                                    colums.setBiaya(biayaFinal);
                                    BigDecimal netto = object.getString("n_netto")==null ? null :
                                            object.getString("n_netto").equals("null")?null : new BigDecimal(object.getDouble("n_netto"));
                                    colums.setNetto(netto);
                                    colums.setTglAntar(object.getString("dt_tglpengantaran")==null ? null :
                                            object.getString("dt_tglpengantaran").equals("null")?null : object.getString("dt_tglpengantaran"));
                                    Integer hariGaransi = object.getString("i_hari_garansi")==null ? null :
                                            object.getString("i_hari_garansi").equals("null")?null : object.getInt("i_hari_garansi");
                                    colums.setHariGaransi(hariGaransi);
                                    colums.setTglBatasGaransi(object.getString("dt_batas_garansi")==null ? null :
                                            object.getString("dt_batas_garansi").equals("null")?null : object.getString("dt_batas_garansi"));
                                    colums.setCdstatus(object.getString("c_dstatus")==null ? null :
                                            object.getString("c_dstatus").equals("null")?null : object.getString("c_dstatus"));
                                    Integer countUlasan = object.getString("count_ulasan")==null ? null :
                                            object.getString("count_ulasan").equals("null")?null : object.getInt("count_ulasan");
                                    colums.setCountUlasan(countUlasan);
                                    Integer countUserUlasan = object.getString("count_user_ulasan")==null ? null :
                                            object.getString("count_user_ulasan").equals("null")?null : object.getInt("count_user_ulasan");
                                    colums.setCountUserUlasan(countUserUlasan);
                                    colums.setNoReferensi(object.getString("c_referensi")==null ? null :
                                            object.getString("c_referensi").equals("null")?null : object.getString("c_referensi"));
                                    if(colums.getKodeUserTeknisi()!=null) {
                                        MasterKaryawanModel karyawanModel = new MasterKaryawanModel();
                                        karyawanModel.setIdKaryawan(colums.getKodeUserTeknisi());
                                        karyawanModel.setNamakary(colums.getNamaUserTeknisi());
                                        karyawanModel.setKota(object.getString("vc_kotalahir"));
                                        karyawanModel.setTelp(object.getString("c_phone"));
                                        karyawanModel.setTgllahir(object.getString("dt_tgllahir"));
                                        karyawanModel.setPathFoto(object.getString("vc_pathfoto"));
                                        colums.setKaryModel(karyawanModel);
                                    }
                                    columnlist.add(colums);
                                }
                            }else{
                                columnlist = new ArrayList<>();
                                tvstatus.setVisibility(View.VISIBLE);
                                tvstatus.setText("Tidak Ada Data");
                                prbstatus.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter.notifyDataSetChanged();
                        adapter		= new AdpTrHistCust(TransHistCustActivity.this, R.layout.adp_tr_hist_cust, columnlist, level, userId);
                        lsvupload.setAdapter(adapter);
                        //lsvupload.invalidate();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check Koneksi Internet Anda");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof AuthFailureError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("AuthFailureError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ServerError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ServerError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof NetworkError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check NetworkError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ParseError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ParseError");
                    prbstatus.setVisibility(View.GONE);
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kodeCust", kodeCust);
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };
        //AppController.getInstance().getRequestQueue().getCache().invalidate(Url, true);
        AppController.getInstance().addToRequestQueue(jsonget);
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist = new ArrayList<TrHistCustModel>();
        adapter		= new AdpTrHistCust(TransHistCustActivity.this, R.layout.adp_tr_hist_cust, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
        getDataUpload(Link.BASE_URL_API+getUpload, userId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_DEPT) {
            if(resultCode == RESULT_OK) {
                columnlist = new ArrayList<TrHistCustModel>();
                adapter		= new AdpTrHistCust(TransHistCustActivity.this, R.layout.adp_tr_hist_cust, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                getDataUpload(Link.BASE_URL_API+getUpload, userId);
            }
        }
    }
}
