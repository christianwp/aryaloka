package com.example.aryaloka.Transaksi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.aryaloka.Adapter.Transaksi.AdpUlasanTeknisi;
import com.example.aryaloka.Model.master.MasterKaryawanModel;
import com.example.aryaloka.Model.transaksi.TrUlasanTeknisiModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.AppController;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

public class ProfileTeknisiActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level;
    private ImageView imgBack, imgFoto;
    private MasterKaryawanModel karyawan;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private ProgressBar prb;
    private TextView txtNama, txtNilai, txtRating, txtCountUser, txtStatus;
    private ScaleRatingBar ratingDatabase;
    private double rating=0;
    private BigDecimal hasilRating = BigDecimal.ZERO;
    private Integer countUser;
    private ListView lsvData;
    private AdpUlasanTeknisi adapter;
    private ArrayList<TrUlasanTeknisiModel> columnlist= new ArrayList<TrUlasanTeknisiModel>();

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_teknisi);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}
        Intent i = getIntent();
        if (i != null){
            try {
                karyawan = (MasterKaryawanModel) i.getSerializableExtra("karyawan");
            }catch (Exception e) {}
        }

        imgBack = (ImageView)findViewById(R.id.imgViewProfileTeknisiBack);
        imgFoto = (ImageView)findViewById(R.id.imgFotoViewProfileTeknisi);
        prb = (ProgressBar) findViewById(R.id.prbFotoViewProfileTeknisi);
        txtNama = (TextView)findViewById(R.id.txtNamaViewProfileTeknisi);
        txtNilai = (TextView)findViewById(R.id.txtNilaiRating);
        txtRating = (TextView)findViewById(R.id.txtUserRating);
        txtCountUser = (TextView)findViewById(R.id.txtUserRating);
        ratingDatabase = (ScaleRatingBar)findViewById(R.id.simpleRatingBar);
        lsvData = (ListView)findViewById(R.id.listProfileUlasanTeknisi);
        txtStatus = (TextView)findViewById(R.id.txtListProfileTeknisiStatus);

        adapter		= new AdpUlasanTeknisi(ProfileTeknisiActivity.this, R.layout.adp_ulasan_teknisi, columnlist, level, userId);
        lsvData.setAdapter(adapter);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(karyawan.getPathFoto()!=null && !karyawan.getPathFoto().equals("")){
            imgFoto.setBackgroundResource(0);
            prb.setVisibility(View.VISIBLE);
            StorageReference ref = storageReference.child("profile/"+karyawan.getPathFoto().trim());
            Glide.with(this /* context */)
                    .load(ref)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            prb.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            prb.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imgFoto);
        }

        txtNama.setText(karyawan.getNamakary());
        getDataUpload(Link.BASE_URL_API+"getRatingTeknisi.php", karyawan.getIdKaryawan());
    }

    private void reloadDataRating(){
        if(rating>0){
            hasilRating = new BigDecimal(rating).divide(new BigDecimal(countUser),1, RoundingMode.HALF_UP);
        }
        if(hasilRating.compareTo(BigDecimal.ZERO) == 0){
            txtNilai.setText("0.0");
        }else{
            txtNilai.setText(String.valueOf(hasilRating));
        }
        txtCountUser.setText(String.valueOf(countUser));
        ratingDatabase.setClickable(false);
        ratingDatabase.setScrollable(false);
        ratingDatabase.setRating(hasilRating.floatValue());
    }

    private void getDataUpload(String Url, final String kodeTeknisi){
        pDialog.setMessage("Loading Data....");
        showDialog();
        JsonObjectRequest jsonget = new JsonObjectRequest(Request.Method.GET, Url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int sucses= response.getInt("success");
                            if (sucses==1){
                                hideDialog();
                                JSONArray JsonArray = response.getJSONArray("uploade");
                                for (int i = 0; i < JsonArray.length(); i++) {
                                    JSONObject object = JsonArray.getJSONObject(i);
                                    rating = object.getInt("ratingku");
                                    countUser = object.getInt("count_user");;
                                    reloadDataRating();
                                }
                                JSONArray JsonArray2 = response.getJSONArray("uploade2");
                                for (int i = 0; i < JsonArray2.length(); i++) {
                                    JSONObject object = JsonArray2.getJSONObject(i);
                                    TrUlasanTeknisiModel colums 	= new TrUlasanTeknisiModel();
                                    colums.setNoTrans(object.getString("c_notransaksi"));
                                    colums.setKodeCust(object.getString("c_user01"));
                                    colums.setNamaCust(object.getString("vc_username"));
                                    colums.setText(object.getString("t_ulasan"));
                                    colums.setBintang(object.getInt("i_bintang"));
                                    colums.setKodeTeknisi(object.getString("c_userid_teknisi"));
                                    columnlist.add(colums);
                                }
                            }else{
                                columnlist = new ArrayList<>();
                                hideDialog();
                                txtStatus.setVisibility(View.VISIBLE);
                                txtStatus.setText("Tidak ada data");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter.notifyDataSetChanged();
                        adapter		= new AdpUlasanTeknisi(ProfileTeknisiActivity.this, R.layout.adp_ulasan_teknisi, columnlist, level, userId);
                        lsvData.setAdapter(adapter);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    hideDialog();
                    Toast.makeText(ProfileTeknisiActivity.this, "Check Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    hideDialog();
                    Toast.makeText(ProfileTeknisiActivity.this, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    hideDialog();
                    Toast.makeText(ProfileTeknisiActivity.this, "Check ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    hideDialog();
                    Toast.makeText(ProfileTeknisiActivity.this, "Check NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    hideDialog();
                    Toast.makeText(ProfileTeknisiActivity.this, "Check ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kodeTeknisi", kodeTeknisi);
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };
        AppController.getInstance().getRequestQueue().getCache().invalidate(Url, true);
        AppController.getInstance().addToRequestQueue(jsonget);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
