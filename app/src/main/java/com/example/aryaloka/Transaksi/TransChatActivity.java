package com.example.aryaloka.Transaksi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.aryaloka.Adapter.Transaksi.AdpChatting;
import com.example.aryaloka.Model.transaksi.ChattingModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.AppController;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.example.aryaloka.Utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransChatActivity extends AppCompatActivity {

    private ImageView imgBack, imgComent;
    private TextInputEditText edKoment;
    private TextView txtStatus;
    private AdpChatting adapter;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private ListView lsvData;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private ArrayList<ChattingModel> columnlist= new ArrayList<ChattingModel>();
    private String getUpload	="chatting_list.php";
    private String noTrans, idCust, idTeknisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        mApiService         = Link.getAPIService();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (i != null) {
            try {
                noTrans = bundle.getString("noTrans", null);
                idCust = bundle.getString("idUser", null);
                idTeknisi = bundle.getString("idTeknisi", null);
            } catch (Exception e) {
            }
        }

        imgBack = (ImageView)findViewById(R.id.imgBackChatting);
        imgComent = (ImageView)findViewById(R.id.imgSendChatting);
        edKoment = (TextInputEditText)findViewById(R.id.edPostingChatting);
        txtStatus = (TextView)findViewById(R.id.tvPostingChatting);
        lsvData = (ListView)findViewById(R.id.lsvPostingChatting);

        adapter		= new AdpChatting(TransChatActivity.this, R.layout.adp_chatting, columnlist, storageReference, userId);
        lsvData.setAdapter(adapter);
        getDataUpload(Link.BASE_URL_API + getUpload, noTrans);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imgComent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    edKoment.setEnabled(false);
                    imgComent.setEnabled(false);
                    proses_simpan(noTrans, edKoment.getText().toString(), userId);
                }
            }
        });

        edKoment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()>0){
                    imgComent.setImageResource(R.mipmap.ic_action_send_yellow_light);
                }else{
                    imgComent.setImageResource(R.mipmap.ic_action_send_holo_light);
                }
            }
        });
    }

    private void proses_simpan(final String noTrans, final String isiKomen, final String userId){
        pDialog.setMessage("Proses Simpan ...");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow = df.format(today);
        mApiService.insertPostingDetail(noTrans, isiKomen, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    Toast.makeText(TransChatActivity.this, "Chatting berhasil dikirim", Toast.LENGTH_LONG).show();
                                    hideDialog();
                                    edKoment.setText("");
                                    edKoment.setEnabled(true);
                                    imgComent.setEnabled(true);
                                    imgComent.setImageResource(R.mipmap.ic_action_send_holo_light);
                                    if(level.equals("K")){
                                        Utils.sendNotification(mApiService, "Chat Aryaloka App", "Ada pesan baru dari teknisi", idCust);
                                    }else if(level.equals("C")){
                                        Utils.sendNotification(mApiService, "Chat Aryaloka App", "Ada pesan baru dari customer", idTeknisi);
                                    }
                                    getDataUpload(Link.BASE_URL_API + getUpload, noTrans);
                                }else{
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(TransChatActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            }catch (JSONException e) {
                                hideDialog();
                                Toast.makeText(TransChatActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }catch (IOException e) {
                                hideDialog();
                                Toast.makeText(TransChatActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }else{
                            Toast.makeText(TransChatActivity.this, "Gagal kirim pesan", Toast.LENGTH_LONG).show();
                            hideDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(TransChatActivity.this, "Gagal kirim pesan", Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                });
    }

    private void getDataUpload(String Url, String noTransaksi){
        txtStatus.setVisibility(View.GONE);
        pDialog.setMessage("Loading Data ...");
        showDialog();
        StringRequest register = new StringRequest(Request.Method.POST, Url, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonrespon = new JSONObject(response);
                    int sucses= jsonrespon.getInt("success");
                    if (sucses==1){
                        txtStatus.setVisibility(View.GONE);
                        hideDialog();
                        adapter.clear();
                        JSONArray JsonArray = jsonrespon.getJSONArray("uploade");
                        for (int i = 0; i < JsonArray.length(); i++) {
                            JSONObject object = JsonArray.getJSONObject(i);
                            ChattingModel colums 	= new ChattingModel();
                            colums.setNoTrans(object.getString("c_notransaksi"));
                            colums.setTanggal(object.getString("dt_tanggal"));
                            colums.setText(object.getString("t_text"));
                            colums.setKodeUser(object.getString("c_userid"));
                            colums.setNamaUser(object.getString("vc_username"));
                            columnlist.add(colums);
                        }
                    }else{
                        txtStatus.setVisibility(View.VISIBLE);
                        txtStatus.setText("Tidak Ada Data");
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText(e.getMessage());
                    hideDialog();
                }
                adapter.notifyDataSetChanged();
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("Check Koneksi Internet Anda");
                    hideDialog();
                } else if (error instanceof AuthFailureError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("AuthFailureError");
                    hideDialog();
                } else if (error instanceof ServerError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("Check ServerError");
                    hideDialog();
                } else if (error instanceof NetworkError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("Check NetworkError");
                    hideDialog();
                } else if (error instanceof ParseError) {
                    txtStatus.setVisibility(View.VISIBLE);
                    txtStatus.setText("Check ParseError");
                    hideDialog();
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("noTrans", noTransaksi);
                params.put("userId", userId);
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    private boolean validate() {
        boolean value;
        if (edKoment.getText().toString().isEmpty()){
            value=false;
            requestFocus(edKoment);
            Toast.makeText(TransChatActivity.this, "Isi Pesan belum diisi", Toast.LENGTH_LONG).show();
        } else if(noTrans == null){
            value=false;
            Toast.makeText(TransChatActivity.this, "Nomor transaksi tidak ditemukan", Toast.LENGTH_LONG).show();
        } else if(idCust == null){
            value=false;
            Toast.makeText(TransChatActivity.this, "ID Customer tidak ditemukan", Toast.LENGTH_LONG).show();
        } else if(idTeknisi == null){
            value=false;
            Toast.makeText(TransChatActivity.this, "ID Teknisi tidak ditemukan", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}