package com.example.aryaloka.Transaksi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.aryaloka.List.View.ListPromo;
import com.example.aryaloka.List.View.ListTipeBarang;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.example.aryaloka.Utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.aryaloka.Utilities.Utils.getDecimalFormattedString;
import static com.example.aryaloka.Utilities.Utils.trimCommaOfString;

public class TransHistCustItemActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status, kodeBrg, kodePromo;
    private ImageView imgBack;
    private TextInputEditText edKeluhan, edKodeBrg, edAlamat, edPromo;
    private TextInputLayout ilKeluhan, ilKodeBrg, ilAlamat, ilPromo;
    private Button btnSave, btnMap;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private CheckBox ckAlamat;
    private LocationManager locationManager ;
    private boolean GpsStatus, alamatManual=false ;
    private double latitude, longtitude;
    private int RESULT_MAP = 5;
    private boolean alamatMaps;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tr_form_service);
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack = (ImageView)findViewById(R.id.imgViewFormServiceBack);
        edKodeBrg = (TextInputEditText) findViewById(R.id.eAddViewFormServiceTipeBarang);
        ilKodeBrg = (TextInputLayout) findViewById(R.id.input_layout_FormService_tipebarang);
        edKeluhan = (TextInputEditText) findViewById(R.id.eAddViewFormServiceKeluhan);
        ilKeluhan = (TextInputLayout) findViewById(R.id.input_layout_FormService_keluhan);
        edAlamat = (TextInputEditText) findViewById(R.id.eAddViewFormServiceAlamat);
        ilAlamat = (TextInputLayout) findViewById(R.id.input_layout_FormService_alamat);
        edPromo = (TextInputEditText) findViewById(R.id.eAddViewFormServicePromo);
        ilPromo = (TextInputLayout) findViewById(R.id.input_layout_FormService_promo);
        btnMap = (Button) findViewById(R.id.bInfoFormServiceMap);
        ckAlamat = (CheckBox) findViewById(R.id.ckFormServiceAlamatManual);
        btnSave = (Button) findViewById(R.id.btnViewFormServiceSave);

        edPromo.setText("0");

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckGpsStatus() ;
                if(GpsStatus == true){
                    Intent i = new Intent(getApplication(), MapsActivity.class);
                    startActivityForResult(i, RESULT_MAP);
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TransHistCustItemActivity.this);
                    builder.setTitle("Perhatian");
                    builder.setMessage("GPS belum diaktifkan\nAktifkan dahulu!")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert);
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        edPromo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                try{
                    edPromo.removeTextChangedListener(this);
                    String value = edPromo.getText().toString();
                    if (value != null && !value.equals("")){
                        if(value.startsWith(".")){
                            edPromo.setText("0.");
                        }
                        String str = edPromo.getText().toString().replaceAll(",", "");
                        Integer val = Integer.parseInt(str);
                        if (!value.equals(""))
                            edPromo.setText(getDecimalFormattedString(val.toString()));
                        edPromo.setSelection(edPromo.getText().toString().length());
                    }
                    edPromo.addTextChangedListener(this);
                    return;
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    edPromo.addTextChangedListener(this);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateKode() && validateKeluhan() && validateAlamat() && validateLongt()){
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(TransHistCustItemActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    insertData(kodeBrg, edKeluhan.getText().toString(), kodePromo, (new BigDecimal(trimCommaOfString(edPromo.getText().toString()))),
                                            latitude, longtitude, edAlamat.getText().toString(), userId);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    android.app.AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        ckAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alamatManual=!alamatManual;
                edAlamat.setEnabled(alamatManual);
            }
        });

        edKodeBrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edKodeBrg.setEnabled(false);
                hideKeyboard(v);
                pilihBarang();
                edKodeBrg.setEnabled(true);
            }
        });

        edKodeBrg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edKodeBrg.setEnabled(false);
                    hideKeyboard(v);
                    pilihBarang();
                    edKodeBrg.setEnabled(true);
                }
            }
        });

        edPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edKodeBrg.getText().toString().isEmpty()){
                    //requestFocus(edKodeBrg);
                    ilKodeBrg.setError(getString(R.string.err_msg_kode));
                }else{
                    edPromo.setEnabled(false);
                    hideKeyboard(v);
                    pilihPromo(edKodeBrg.getText().toString());
                    edPromo.setEnabled(true);
                }
            }
        });

        edPromo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    if(edKodeBrg.getText().toString().isEmpty()){
                        //requestFocus(edKodeBrg);
                        ilKodeBrg.setError(getString(R.string.err_msg_kode));
                    }else{
                        edPromo.setEnabled(false);
                        hideKeyboard(v);
                        pilihPromo(edKodeBrg.getText().toString());
                        edPromo.setEnabled(true);
                    }
                }
            }
        });
    }

    private void insertData(String kodeBarng, String keluhan, String kodePromo, BigDecimal nilaiPromo, double latt,
                            double longt, String alamat, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        String tanggalNow = df.format(today);
        Integer year = cal.get(Calendar.YEAR);
        Integer month = cal.get(Calendar.MONTH)+1;
        mApiService.insertService(kodeBarng, keluhan, kodePromo, nilaiPromo, latt, longt, alamat, tanggalNow, userId, year, month)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(TransHistCustItemActivity.this, "DATA BERHASIL DISIMPAN.", Toast.LENGTH_LONG).show();

                                    String idTeknisi = jsonRESULTS.getString("idTeknisi");
                                    Utils.sendNotification(mApiService, "Aryaloka App", "Ada Transaksi baru yang masuk", idTeknisi);
                                    Utils.sendNotification(mApiService, "Aryaloka App", "Teknisi akan segera ke lokasi anda. Harap menunggu!", userId);

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(TransHistCustItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(TransHistCustItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(TransHistCustItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(TransHistCustItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(TransHistCustItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void pilihBarang(){
        Intent i = new Intent(TransHistCustItemActivity.this, ListTipeBarang.class);
        startActivityForResult(i, 2);
    }

    private void pilihPromo(String kodeBrg){
        Intent i = new Intent(TransHistCustItemActivity.this, ListPromo.class);
        i.putExtra("kodeBarang", kodeBrg);
        startActivityForResult(i, 3);
    }

    private boolean validateAlamat() {
        boolean value;
        if (edAlamat.getText().toString().isEmpty()){
            value=false;
            requestFocus(edAlamat);
            ilAlamat.setError(getString(R.string.err_msg_alamat));
        } else {
            value=true;
            ilAlamat.setError(null);
        }
        return value;
    }

    private boolean validateKode() {
        boolean value;
        if (edKodeBrg.getText().toString().isEmpty()){
            value=false;
            requestFocus(edKodeBrg);
            ilKodeBrg.setError(getString(R.string.err_msg_kode));
        } else if (edKodeBrg.length() > ilKodeBrg.getCounterMaxLength()) {
            value=false;
            ilKodeBrg.setError("Max character length is " + ilKodeBrg.getCounterMaxLength());
        } else {
            value=true;
            ilKodeBrg.setError(null);
        }
        return value;
    }

    private boolean validateLongt() {
        boolean value;
        Double latt = new Double(latitude);
        Double longt = new Double(longtitude);
        if (latt == null || longt == null){
            value=false;
            Toast.makeText(TransHistCustItemActivity.this, "Harap set lokasi map", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private boolean validateKeluhan() {
        boolean value;
        if (edKeluhan.getText().toString().isEmpty()){
            value=false;
            requestFocus(edKeluhan);
            ilKeluhan.setError(getString(R.string.err_msg_keluhan));
        } else if (edKeluhan.length() > ilKeluhan.getCounterMaxLength()) {
            value=false;
            ilKeluhan.setError("Max character length is " + ilKeluhan.getCounterMaxLength());
        } else {
            value=true;
            ilKeluhan.setError(null);
        }
        return value;
    }

    public void CheckGpsStatus(){
        locationManager = (LocationManager)TransHistCustItemActivity.this.getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 2) {
            if(resultCode == RESULT_OK) {
                edKodeBrg.setText(data.getStringExtra("nama"));
                kodeBrg = data.getStringExtra("kode");
            }
        }
        if(requestCode == 3) {
            if(resultCode == RESULT_OK) {
                edPromo.setText(data.getStringExtra("nilai"));
                kodePromo = data.getStringExtra("kode");
            }
        }
        if (requestCode == RESULT_MAP) {//map
            if(resultCode == RESULT_OK) {//map
                latitude = data.getDoubleExtra("latitude",0);
                longtitude = data.getDoubleExtra("longtitude",0);
                edAlamat.setText(data.getStringExtra("alamat"));
                alamatMaps=true;
            }else{
                alamatMaps=false;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}