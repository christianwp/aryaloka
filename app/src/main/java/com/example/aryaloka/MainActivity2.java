package com.example.aryaloka;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.aryaloka.Utilities.PrefUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

public class MainActivity2 extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private BottomNavigationView bottomNavigationView;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String userId, username, telp, email, status, paswd;
    private FirebaseAnalytics mFirebaseAnalytics;
    private static final String TAG = "MainActivity2";

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity2);
        pref = new PrefUtil(this);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            username = shared.getString(PrefUtil.NAME, null);
            telp = shared.getString(PrefUtil.TELP, null);
            email = shared.getString(PrefUtil.EMAIL, null);
            status = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){
            e.getMessage();
            //Crashlytics.logException(new Exception(e.getMessage()));
        }

        FirebaseApp.initializeApp(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        FirebaseMessaging.getInstance().subscribeToTopic(userId)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "";
                        if (!task.isSuccessful()) {
                            msg = "Koneksi Notifikasi Gagal:("+userId+")";
                        }
                        Log.d(TAG, msg);
                        Toast.makeText(MainActivity2.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMain2);
        toolbar.setTitle("Aryaloka App");
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(R.id.new_menu_master);
    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_menu_master:
                changeFragmentListUploadKriteria(new FragmentMaster(), userId, username, telp, email, status);
                return true;
            case R.id.new_menu_trans:
                changeFragmentListUploadKriteria(new FragmentTransaksi(), userId, username, telp, email, status);
                return true;
        }
        return false;
    }

    private void changeFragmentListUploadKriteria(Fragment targetFragment, String userId, String username,
                                                  String telp, String email, String status){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flFragment, targetFragment)
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .commit();
        Bundle extras = new Bundle();
        extras.putString("userId", userId);
        extras.putString("username", username);
        extras.putString("telp", telp);
        extras.putString("email", email);
        extras.putString("status", status);
        targetFragment.setArguments(extras);
    }
}