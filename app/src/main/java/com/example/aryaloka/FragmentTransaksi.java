package com.example.aryaloka;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.aryaloka.Transaksi.TransHistCustActivity;
import com.example.aryaloka.Transaksi.TransHistCustItemActivity;
import com.example.aryaloka.Transaksi.TransHistTeknisiActivity;
import com.example.aryaloka.Utilities.PrefUtil;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.fragment.app.Fragment;

public class FragmentTransaksi extends Fragment {

    private View vupload;
    private LinearLayout linTrServis, linTrHistory, linLogout;
    private String userId, username, telp, email, status, paswd;
    private PrefUtil pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle	 = this.getArguments();
        if (bundle!=null){
            userId	= bundle.getString("userId");
            username	= bundle.getString("username");
            telp	= bundle.getString("telp");
            email	= bundle.getString("email");
            status	= bundle.getString("status");
        }
        vupload     = inflater.inflate(R.layout.frg_transaksi, container,false);
        linTrServis = (LinearLayout)vupload.findViewById(R.id.linTrServis);
        linTrHistory = (LinearLayout)vupload.findViewById(R.id.linTrHistory);
        linLogout = (LinearLayout)vupload.findViewById(R.id.linTrLogout);

        if(status.equals("A")){
            linTrServis.setVisibility(View.GONE);
            linTrHistory.setVisibility(View.VISIBLE);
        }else if(status.equals("C")){
            linTrServis.setVisibility(View.VISIBLE);
            linTrHistory.setVisibility(View.VISIBLE);
        }else if(status.equals("K")){
            linTrServis.setVisibility(View.GONE);
            linTrHistory.setVisibility(View.VISIBLE);
        }

        linTrServis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), TransHistCustItemActivity.class));
            }
        });

        linTrHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(status.equals("C")) {
                    startActivity(new Intent(getContext(), TransHistCustActivity.class));
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else if(status.equals("K")) {
                    startActivity(new Intent(getContext(), TransHistTeknisiActivity.class));
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else if(status.equals("A")) {
                    startActivity(new Intent(getContext(), TransHistTeknisiActivity.class));
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    Toast.makeText(getContext(), "INVALID STATUS USER", Toast.LENGTH_LONG).show();
                }
            }
        });

        linLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(userId);
                startActivity(new Intent(getContext(), LoginActivity.class));
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                getActivity().finish();
            }
        });

        return vupload;
    }
}
