package com.example.aryaloka;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.example.aryaloka.Utilities.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private BaseApiService mApiService, mUploadService;
    private ProgressDialog pDialog;
    private PrefUtil prefUtil;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private static final int REQUEST_CODE_GALLERY = 0013;
    private String hasilFoto = "N", encodedString;
    private Uri selectedImage;
    private Date tglLahir;
    private TextInputEditText eUserId, eNama, ePassword, ePassword2, eTelp, eEmail;
    private TextInputLayout inputLayoutUserId, inputLayoutNama, inputLayoutPasw, inputLayoutPasw2, inputLayoutTelp,
            inputLayoutEmail;
    private ImageView imgFoto;
    private Button btnRegister, btnClearUserId;
    private Calendar dateAndTime = Calendar.getInstance();
    private FirebaseStorage storage;
    private StorageReference storageReference;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        mApiService         = Link.getAPIService();
        mUploadService      = Link.getImgProfileService();
        prefUtil            = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        btnRegister = (Button) findViewById(R.id.bRegisterDaftar);
        btnClearUserId = (Button) findViewById(R.id.btnRegisterClearUserID);
        imgFoto = (ImageView) findViewById(R.id.imgFotoProfile);
        inputLayoutUserId = (TextInputLayout)findViewById(R.id.input_layout_register_userid);
        inputLayoutNama = (TextInputLayout)findViewById(R.id.input_layout_register_nama);
        inputLayoutPasw = (TextInputLayout)findViewById(R.id.input_layout_register_sandi);
        inputLayoutPasw2 = (TextInputLayout)findViewById(R.id.input_layout_register_sandi2);
        inputLayoutTelp = (TextInputLayout)findViewById(R.id.input_layout_register_telp);
        inputLayoutEmail = (TextInputLayout)findViewById(R.id.input_layout_register_email);

        eUserId = (TextInputEditText)findViewById(R.id.eRegisterUserId);
        eNama = (TextInputEditText)findViewById(R.id.eRegisterNama);
        ePassword = (TextInputEditText)findViewById(R.id.eRegisterSandi);
        ePassword2 = (TextInputEditText)findViewById(R.id.eRegisterSandi2);
        eTelp = (TextInputEditText)findViewById(R.id.eRegisterTelp);
        eEmail = (TextInputEditText)findViewById(R.id.eRegisterEmail);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        btnClearUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eUserId.setText("");
                btnClearUserId.setVisibility(View.INVISIBLE);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUserId(eUserId.length()) && validateNama(eNama.length()) &&
                        validatePasw(ePassword.length()) && validatePasw2(ePassword2.length()) &&
                        validateTelp(eTelp.length()) && validateEmail(eEmail.length())){
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(hasilFoto.equals("N")){
                                        requestRegister(eNama.getText().toString(), ePassword.getText().toString(), eUserId.getText().toString(),
                                                eTelp.getText().toString(), eEmail.getText().toString());
                                    }else{
                                        uploadImage();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        imgFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void openImage() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_CODE_GALLERY);
    }

    private void requestRegister(String nama, String pasw, String id, String telp, String email){
        pDialog.setMessage("Registering ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.registerRequest(id, pasw, nama, tanggalNow, telp, email, "C",
                hasilFoto.equals("N")?null:eUserId.getText().toString()+".jpg")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(RegisterActivity.this, "BERHASIL REGISTRASI", Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(RegisterActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(RegisterActivity.this, "REGISTRASI GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(RegisterActivity.this, "REGISTRASI GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(RegisterActivity.this, "REGISTRASI GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(RegisterActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean validateUserId(int length) {
        boolean value;
        if (eUserId.getText().toString().isEmpty()){
            value=false;
            requestFocus(eUserId);
            inputLayoutUserId.setError(getString(R.string.err_msg_user));
        } else if (length > inputLayoutUserId.getCounterMaxLength()) {
            value=false;
            inputLayoutUserId.setError("Max character User ID length is " + inputLayoutUserId.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutUserId.setError(null);
        }
        return value;
    }

    private boolean validatePasw(int length) {
        boolean value=true;
        int minValue = 6;
        if (ePassword.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(ePassword);
            inputLayoutPasw.setError(getString(R.string.err_msg_sandi));
        } else if (length > inputLayoutPasw.getCounterMaxLength()) {
            value=false;
            inputLayoutPasw.setError("Max character password length is " + inputLayoutPasw.getCounterMaxLength());
        } else if (length < minValue) {
            value=false;
            inputLayoutPasw.setError("Min character password length is 6" );
        } else{
            value=true;
            inputLayoutPasw.setError(null);}
        return value;
    }

    private boolean validatePasw2(int length) {
        boolean value=true;
        int minValue = 6;
        if (ePassword2.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(ePassword2);
            inputLayoutPasw2.setError(getString(R.string.err_msg_sandi));
        } else if (length > inputLayoutPasw2.getCounterMaxLength()) {
            value=false;
            inputLayoutPasw2.setError("Max character password length is " + inputLayoutPasw2.getCounterMaxLength());
        } else if (length < minValue) {
            value=false;
            inputLayoutPasw2.setError("Min character password length is 6" );
        } else if(!ePassword2.getText().toString().equals(ePassword.getText().toString())){
            value=false;
            inputLayoutPasw2.setError("Confirm Password is wrong");
        }else{
            value=true;
            inputLayoutPasw2.setError(null);
        }
        return value;
    }

    private boolean validateTelp(int length) {
        boolean value=true;
        if (eTelp.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eTelp);
            inputLayoutTelp.setError(getString(R.string.err_msg_telp));
        } else if (length > inputLayoutTelp.getCounterMaxLength()) {
            value=false;
            inputLayoutTelp.setError("Max character name length is " + inputLayoutTelp.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutTelp.setError(null);
        }
        return value;
    }

    private boolean validateNama(int length) {
        boolean value=true;
        if (eNama.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eNama);
            inputLayoutNama.setError(getString(R.string.err_msg_nama));
        } else if (length > inputLayoutNama.getCounterMaxLength()) {
            value=false;
            inputLayoutNama.setError("Max character name length is " + inputLayoutNama.getCounterMaxLength());
        }else {
            value=true;
            inputLayoutNama.setError(null);
        }
        return value;
    }

    private boolean validateEmail(int length) {
        boolean value=true;
        if (eEmail.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(eEmail);
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
        } else if (length > inputLayoutEmail.getCounterMaxLength()) {
            value=false;
            inputLayoutEmail.setError("Max character name length is " + inputLayoutEmail.getCounterMaxLength());
        }else {
            value= Utils.isEmailValid(eEmail.getText().toString());
            if(value){
                inputLayoutEmail.setError(null);
            }else{
                inputLayoutEmail.setError("Email Tidak Valid");
            }
        }
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        Utils.freeMemory();
        super.onDestroy();
        Utils.trimCache(this);
    }

    private void uploadImage() {
        if (selectedImage != null) {
            pDialog.setMessage("Uploading Image...");
            showDialog();
            StorageReference ref = storageReference.child("profile/"+ (eUserId.getText().toString()+".jpg"));

            ref.putFile(selectedImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onSuccess(
                                UploadTask.TaskSnapshot taskSnapshot){
                            requestRegister(eNama.getText().toString(), ePassword.getText().toString(), eUserId.getText().toString(),
                                    eTelp.getText().toString(), eEmail.getText().toString());
                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure( Exception e){
                            hideDialog();
                            Toast.makeText(RegisterActivity.this,"Failed " + e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot){
                                }
                            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                hasilFoto = "Y";
                selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                /*Utils.getCycleImage("file:///"+picturePath, imgFoto, this);
                String fileNameSegments[] = picturePath.split("/");
                Bitmap myImg = BitmapFactory.decodeFile(picturePath);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // Must compress the Image to reduce image size to make upload easy
                myImg.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byte_arr = stream.toByteArray();
                // Encode Image to String
                encodedString = Base64.encodeToString(byte_arr, Base64.DEFAULT);*/
                imgFoto.setBackgroundResource(0);
                Glide.with(this)
                        .load(Uri.parse("file:///"+picturePath))
                        .into(imgFoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
