package com.example.aryaloka.Service;

import com.example.aryaloka.Utilities.JSONResponse;

import java.math.BigDecimal;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by christian on 14/02/18.
 */

public interface BaseApiService {

    @FormUrlEncoded
    @POST("login.php")
    Call<ResponseBody> loginRequest(@Field("userid") String id,
                                    @Field("password") String pasw);

    @FormUrlEncoded
    @POST("register.php")
    Call<ResponseBody> registerRequest(@Field("userid") String id,
                                       @Field("password") String pasw,
                                       @Field("username") String nama,
                                       @Field("tglNow") String tanggalNow,
                                       @Field("telp") String telp,
                                       @Field("email") String email,
                                       @Field("status") String status,
                                       @Field("path") String path);

    @FormUrlEncoded
    @POST("barang_insert.php")
    Call<ResponseBody> insertTipeBarang(@Field("kode") String kode,
                                        @Field("nama") String nama,
                                        @Field("tglNow") String tanggalNow,
                                        @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("barang_update.php")
    Call<ResponseBody> updateTipeBarang(@Field("kode") String kode,
                                        @Field("nama") String nama,
                                        @Field("tglNow") String tanggalNow,
                                        @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("keahlian_insert.php")
    Call<ResponseBody> insertKeahlianKaryawan(@Field("kodeKary") String kodeKary,
                                      @Field("kodeBarang") String kodeBarang,
                                      @Field("nilai") BigDecimal nilai,
                                      @Field("tglNow") String tanggalNow,
                                      @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("keahlian_update.php")
    Call<ResponseBody> updateKeahlianKaryawan(@Field("kodeKary") String kodeKary,
                                              @Field("kodeBarang") String kodeBarang,
                                              @Field("nilai") BigDecimal nilai,
                                              @Field("tglNow") String tanggalNow,
                                              @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("chatting_insert.php")
    Call<ResponseBody> insertPostingDetail(@Field("noTrans") String noTrans,
                                           @Field("isi") String isi,
                                           @Field("tglNow") String tanggalNow,
                                           @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("service_update.php")
    Call<ResponseBody> updateServis1(@Field("noTrans") String noTrans,
                                     @Field("status") String status,
                                     @Field("ket") String ket,
                                     @Field("est_biaya") BigDecimal est_biaya,
                                     @Field("est_hari") Integer est_hari,
                                     @Field("tglNow") String tanggalNow,
                                     @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("service_update.php")
    Call<ResponseBody> updateServis2(@Field("noTrans") String noTrans,
                                     @Field("status") String status,
                                     @Field("ket") String ket,
                                     @Field("biaya_final") BigDecimal biaya,
                                     @Field("tglNow") String tanggalNow,
                                     @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("service_update.php")
    Call<ResponseBody> updateServis3(@Field("noTrans") String noTrans,
                                     @Field("status") String status,
                                     @Field("ket") String ket,
                                     @Field("hari_garansi") Integer hariGaransi,
                                     @Field("tgl_max_garansi") String tglGaransi,
                                     @Field("tglNow") String tanggalNow,
                                     @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("karyawan_insert.php")
    Call<ResponseBody> insertKaryawan(@Field("kode") String kode,
                                      @Field("nama") String nama,
                                      @Field("telp") String telp,
                                      @Field("tglLahir") String tglLahir,
                                      @Field("kota") String kota,
                                      @Field("tahun") Integer tahun,
                                      @Field("bulan") Integer bulan,
                                      @Field("usingPhoto") boolean usingPhoto,
                                      @Field("tglNow") String tanggalNow,
                                      @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("karyawan_update.php")
    Call<ResponseBody> updateKaryawan(@Field("kode") String kode,
                                      @Field("nama") String nama,
                                      @Field("telp") String telp,
                                      @Field("tglLahir") String tglLahir,
                                      @Field("kota") String kota,
                                      @Field("tglNow") String tanggalNow,
                                      @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("service_insert.php")
    Call<ResponseBody> insertService(@Field("kodeBarang") String kodeBarang,
                                     @Field("keluhan") String keluhan,
                                     @Field("kodePromo") String kodePromo,
                                     @Field("nilai") BigDecimal nilai,
                                     @Field("latt") double latt,
                                     @Field("longt") double longt,
                                     @Field("alamat") String alamat,
                                     @Field("tglNow") String tanggalNow,
                                     @Field("kodeUser") String kodeUser,
                                     @Field("tahun") Integer tahun,
                                     @Field("bulan") Integer bulan);

    @FormUrlEncoded
    @POST("garansi_insert.php")
    Call<ResponseBody> insertGaransi(@Field("kodeBarang") String kodeBarang,
                                     @Field("keluhan") String keluhan,
                                     @Field("referensi") String referensi,
                                     @Field("latt") double latt,
                                     @Field("longt") double longt,
                                     @Field("alamat") String alamat,
                                     @Field("tglNow") String tanggalNow,
                                     @Field("kodeUser") String kodeUser,
                                     @Field("tahun") Integer tahun,
                                     @Field("bulan") Integer bulan);

    @FormUrlEncoded
    @POST("promo_insert.php")
    Call<ResponseBody> insertPromo(@Field("kodeBarang") String kodeBarang,
                                   @Field("nama") String nama,
                                   @Field("nominal") BigDecimal nominal,
                                   @Field("tglFrom") String tglFrom,
                                   @Field("tglTo") String tglTo,
                                   @Field("tahun") Integer tahun,
                                   @Field("bulan") Integer bulan,
                                   @Field("tglNow") String tanggalNow,
                                   @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("promo_update.php")
    Call<ResponseBody> updatePromo(@Field("kodePromo") String kodePromo,
                                   @Field("nama") String nama,
                                   @Field("kodeBarang") String kodeBarang,
                                   @Field("nilai") BigDecimal nominal,
                                   @Field("tglFrom") String tglFrom,
                                   @Field("tglTo") String tglTo,
                                   @Field("tglNow") String tanggalNow,
                                   @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("user_update.php")
    Call<ResponseBody> updateUser(@Field("kode") String kode,
                                  @Field("pasw") String pasw,
                                  @Field("stat") String stat,
                                  @Field("tglNow") String tanggalNow,
                                  @Field("kodeUser") String kodeUser);

    @FormUrlEncoded
    @POST("dialogRate.php")
    Call<ResponseBody> saveRating(@Field("noTrans") String noTrans,
                                  @Field("idTeknisi") String kodeUserTeknisi,
                                  @Field("idUser") String idUser,
                                  @Field("isi") String isi,
                                  @Field("rate") double rate,
                                  @Field("device") String device,
                                  @Field("tglNow") String tglNow);

    @FormUrlEncoded
    @POST("notification.php")
    Call<ResponseBody> sendNotification(@Field("judul") String judul,
                                        @Field("message") String message,
                                        @Field("topics") String topics);

    @FormUrlEncoded
    @POST("listTipeBarang.php")
    Call<JSONResponse> getListTipeBarang(@Field("level") String level);

    @FormUrlEncoded
    @POST("listPromo.php")
    Call<JSONResponse> getListPromo(@Field("kodeBarang") String kodeBarang);
}