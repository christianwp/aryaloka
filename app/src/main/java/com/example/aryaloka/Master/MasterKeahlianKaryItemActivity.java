package com.example.aryaloka.Master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.aryaloka.List.View.ListTipeBarang;
import com.example.aryaloka.Model.master.MasterKaryawanModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.aryaloka.Utilities.Utils.trimCommaOfString;

public class MasterKeahlianKaryItemActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private ProgressBar prb;
    private String userId, level, status;
    private ImageView imgBack;
    private TextInputEditText edNilai, edTipeBrg;
    private TextInputLayout ilNilai, ilTipeBrg;
    private Button btnSave;
    private String kodeBrg, namaBrg, kodeKary;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_keahlian_karyawan_item);
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (i != null) {
            try {
                status = bundle.getString("Status");// ADD/EDIT
                kodeKary = bundle.getString("kodeKary");
            } catch (Exception e) {
            }
        }
        prefUtil = new PrefUtil(this);
        mApiService = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try {
            shared = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        } catch (Exception e) {
            e.getMessage();
        }

        imgBack = (ImageView) findViewById(R.id.imgViewKeahlianItemBack);
        edNilai = (TextInputEditText) findViewById(R.id.eAddViewKeahlianNilai);
        ilNilai = (TextInputLayout) findViewById(R.id.input_layout_keahlian_nilai);
        edTipeBrg = (TextInputEditText) findViewById(R.id.eAddViewKeahlianTipeBarang);
        ilTipeBrg = (TextInputLayout) findViewById(R.id.input_layout_keahlian_tipebarang);
        btnSave = (Button) findViewById(R.id.btnViewKeahlianSave);

        if(status.equals("EDIT")){
            namaBrg = bundle.getString("namaBrg");
            kodeBrg = bundle.getString("kodeBrg");
            edTipeBrg.setText(namaBrg);
            edNilai.setText(bundle.getString("nilai"));
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateKode() && validateNilai()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MasterKeahlianKaryItemActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (status.equals("ADD")) {
                                        insertData(kodeKary, kodeBrg, new BigDecimal(edNilai.getText().toString()), userId);
                                    } else if (status.equals("EDIT")) {
                                        updateData(kodeKary, kodeBrg, new BigDecimal(edNilai.getText().toString()), userId);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

        edTipeBrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edTipeBrg.setEnabled(false);
                hideKeyboard(v);
                pilihBarang();
                edTipeBrg.setEnabled(true);
            }
        });

        edTipeBrg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edTipeBrg.setEnabled(false);
                    hideKeyboard(v);
                    pilihBarang();
                    edTipeBrg.setEnabled(true);
                }
            }
        });
    }

    private void insertData(String kodeKary, String kodeBarang, BigDecimal nilai, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        String tanggalNow = df.format(today);
        mApiService.insertKeahlianKaryawan(kodeKary, kodeBarang, nilai, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MasterKeahlianKaryItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MasterKeahlianKaryItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterKeahlianKaryItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterKeahlianKaryItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MasterKeahlianKaryItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MasterKeahlianKaryItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateData(String kodeKary, String kodeBarang, BigDecimal nilai, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.updateKeahlianKaryawan(kodeKary, kodeBarang, nilai, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MasterKeahlianKaryItemActivity.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MasterKeahlianKaryItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterKeahlianKaryItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterKeahlianKaryItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MasterKeahlianKaryItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MasterKeahlianKaryItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void pilihBarang(){
        Intent i = new Intent(MasterKeahlianKaryItemActivity.this, ListTipeBarang.class);
        startActivityForResult(i, 2);
    }

    private boolean validateKode() {
        boolean value;
        if (edTipeBrg.getText().toString().isEmpty()){
            value=false;
            requestFocus(edTipeBrg);
            ilTipeBrg.setError(getString(R.string.err_msg_kode));
        } else if (edTipeBrg.length() > ilTipeBrg.getCounterMaxLength()) {
            value=false;
            ilTipeBrg.setError("Max character length is " + ilTipeBrg.getCounterMaxLength());
        } else {
            value=true;
            ilTipeBrg.setError(null);
        }
        return value;
    }

    private boolean validateNilai() {
        boolean value;
        if (edNilai.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNilai);
            ilNilai.setError(getString(R.string.err_msg_nilai));
        } else if(new BigDecimal(trimCommaOfString(edNilai.getText().toString())).compareTo(BigDecimal.ZERO)<=0){
            value=false;
            ilNilai.setError("Nominal tidak valid");
        } else if (edNilai.length() > ilNilai.getCounterMaxLength()) {
            value=false;
            ilNilai.setError("Max character length is " + ilNilai.getCounterMaxLength());
        } else {
            value=true;
            ilNilai.setError(null);
        }
        return value;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        if(requestCode == 2) {
            if(resultCode == RESULT_OK) {
                edTipeBrg.setText(data.getStringExtra("nama"));
                kodeBrg = data.getStringExtra("kode");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
