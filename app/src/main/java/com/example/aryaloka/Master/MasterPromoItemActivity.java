package com.example.aryaloka.Master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.aryaloka.List.View.ListTipeBarang;
import com.example.aryaloka.Model.master.MasterPromoModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.example.aryaloka.Utilities.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.aryaloka.Utilities.Utils.getDecimalFormattedString;
import static com.example.aryaloka.Utilities.Utils.trimCommaOfString;

public class MasterPromoItemActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status, kodeBrg;
    private ImageView imgBack, imgTglFrom, imgTglTo;
    private TextInputEditText edNama, edKodeBrg, edNilai, edTglFrom, edTglTo;
    private TextInputLayout ilNama, ilKodeBrg, ilNilai;
    private Button btnSave;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private MasterPromoModel model;
    private Calendar dateAndTime = Calendar.getInstance();
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_promo_item_activity);
        Intent i = getIntent();
        if (i != null){
            try {
                status = i.getExtras().getString("Status");// ADD/EDIT
                model = (MasterPromoModel)i.getSerializableExtra("model");
            } catch (Exception e) {}
        }
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack = (ImageView)findViewById(R.id.imgViewPromoBack);
        imgTglFrom = (ImageView)findViewById(R.id.imgViewPromoTglFrom);
        imgTglTo = (ImageView)findViewById(R.id.imgViewPromoTglTo);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewPromoNama);
        ilNama = (TextInputLayout) findViewById(R.id.input_layout_promo_nama);
        edKodeBrg = (TextInputEditText) findViewById(R.id.eAddViewPromoTipeBarang);
        ilKodeBrg = (TextInputLayout) findViewById(R.id.input_layout_promo_tipebarang);
        edNilai = (TextInputEditText) findViewById(R.id.eAddViewPromoNilai);
        ilNilai = (TextInputLayout) findViewById(R.id.input_layout_promo_nilai);
        edTglFrom = (TextInputEditText) findViewById(R.id.eAddViewPromoTglFrom);
        edTglTo = (TextInputEditText) findViewById(R.id.eAddViewPromoTglTo);
        btnSave = (Button) findViewById(R.id.btnViewPromoSave);

        if(status.equals("EDIT")){
            Calendar cal = Calendar.getInstance();
            edNama.setText(model.getNamaPromo());
            edKodeBrg.setText(model.getNamaBarang());
            kodeBrg = model.getKodeBarang();
            edNilai.setText(String.valueOf(formatter.format(model.getNilai().setScale(0))));
            Date tglFrom = cal.getTime();
            Date tglTo = cal.getTime();
            try{
                tglFrom = df.parse(model.getTglFrom());
                tglTo = df.parse(model.getTglTo());
            }catch (Exception ex){}
            edTglFrom.setText(sdf1.format(tglFrom));
            edTglTo.setText(sdf1.format(tglTo));
        }else{
            model = new MasterPromoModel();
            edNilai.setText("0");
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        imgTglFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MasterPromoItemActivity.this, dFrom, dateAndTime.get(Calendar.YEAR), dateAndTime.get(Calendar.MONTH),
                        dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        imgTglTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MasterPromoItemActivity.this, dTo, dateAndTime.get(Calendar.YEAR), dateAndTime.get(Calendar.MONTH),
                        dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        edKodeBrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edKodeBrg.setEnabled(false);
                hideKeyboard(v);
                pilihBarang();
                edKodeBrg.setEnabled(true);
            }
        });

        edKodeBrg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    edKodeBrg.setEnabled(false);
                    hideKeyboard(v);
                    pilihBarang();
                    edKodeBrg.setEnabled(true);
                }
            }
        });

        edNilai.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                try{
                    edNilai.removeTextChangedListener(this);
                    String value = edNilai.getText().toString();
                    if (value != null && !value.equals("")){
                        if(value.startsWith(".")){
                            edNilai.setText("0.");
                        }
                        /*if(value.startsWith("0") && !value.startsWith("0.")){
                            edNominal.setText("");
                        }*/
                        String str = edNilai.getText().toString().replaceAll(",", "");
                        Integer val = Integer.parseInt(str);
                        if (!value.equals(""))
                            edNilai.setText(getDecimalFormattedString(val.toString()));
                        edNilai.setSelection(edNilai.getText().toString().length());
                    }
                    edNilai.addTextChangedListener(this);
                    return;
                }
                catch (Exception ex){
                    ex.printStackTrace();
                    edNilai.addTextChangedListener(this);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateNama() && validateKode() && validateNilai() && validateTglFrom() && validateTglTo()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MasterPromoItemActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    model.setNamaPromo(edNama.getText().toString());
                                    model.setNilai(new BigDecimal(trimCommaOfString(edNilai.getText().toString())));
                                    Date tglFrom = Calendar.getInstance().getTime();
                                    Date tglTo = Calendar.getInstance().getTime();
                                    try{
                                        tglFrom = Utils.getFirstTimeOfDay(sdf1.parse(edTglFrom.getText().toString()));
                                        tglTo = Utils.getLastTimeOfDay(sdf1.parse(edTglTo.getText().toString()));
                                    }catch (Exception ex){}
                                    model.setTglFrom(df.format(tglFrom));
                                    model.setTglTo(df.format(tglTo));
                                    model.setNamaBarang(edKodeBrg.getText().toString());
                                    model.setKodeBarang(kodeBrg);
                                    if(status.equals("ADD")){
                                        insertData(model, userId);
                                    }else if(status.equals("EDIT")){
                                        updateData(model, userId);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
    }

    private DatePickerDialog.OnDateSetListener dFrom =new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, month);
            dateAndTime.set(Calendar.DAY_OF_MONTH, day);
            updatelabelFrom();
        }
    };

    private void updatelabelFrom(){
        edTglFrom.setText(sdf1.format(dateAndTime.getTime()));
    }

    private DatePickerDialog.OnDateSetListener dTo =new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, month);
            dateAndTime.set(Calendar.DAY_OF_MONTH, day);
            updatelabelTo();
        }
    };

    private void updatelabelTo(){
        edTglTo.setText(sdf1.format(dateAndTime.getTime()));
    }

    private void insertData(MasterPromoModel model, String userId){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        String tanggalNow = df.format(today);
        Integer year = cal.get(Calendar.YEAR);
        Integer month = cal.get(Calendar.MONTH)+1;
        mApiService.insertPromo(model.getKodeBarang(), model.getNamaPromo(), model.getNilai(), model.getTglFrom(),
                model.getTglTo(), year, month, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MasterPromoItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MasterPromoItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterPromoItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterPromoItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MasterPromoItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MasterPromoItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateData(MasterPromoModel model, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.updatePromo(model.getKodePromo(), model.getNamaPromo(), model.getKodeBarang(), model.getNilai(),
                model.getTglFrom(), model.getTglTo(), tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MasterPromoItemActivity.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MasterPromoItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterPromoItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterPromoItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MasterPromoItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MasterPromoItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void pilihBarang(){
        Intent i = new Intent(MasterPromoItemActivity.this, ListTipeBarang.class);
        startActivityForResult(i, 2);
    }

    private boolean validateNama() {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNama);
            ilNama.setError(getString(R.string.err_msg_nama));
        } else if (edNama.length() > ilNama.getCounterMaxLength()) {
            value=false;
            ilNama.setError("Max character length is " + ilNama.getCounterMaxLength());
        } else {
            value=true;
            ilNama.setError(null);
        }
        return value;
    }

    private boolean validateKode() {
        boolean value;
        if (edKodeBrg.getText().toString().isEmpty()){
            value=false;
            requestFocus(edKodeBrg);
            ilKodeBrg.setError(getString(R.string.err_msg_kode));
        } else if (edKodeBrg.length() > ilKodeBrg.getCounterMaxLength()) {
            value=false;
            ilKodeBrg.setError("Max character length is " + ilKodeBrg.getCounterMaxLength());
        } else {
            value=true;
            ilKodeBrg.setError(null);
        }
        return value;
    }

    private boolean validateNilai() {
        boolean value;
        if (edNilai.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNilai);
            ilNilai.setError(getString(R.string.err_msg_nilai));
        } else if(new BigDecimal(trimCommaOfString(edNilai.getText().toString())).compareTo(BigDecimal.ZERO)<=0){
            value=false;
            ilNilai.setError("Nominal tidak valid");
        } else if (edNilai.length() > ilNilai.getCounterMaxLength()) {
            value=false;
            ilNilai.setError("Max character length is " + ilNilai.getCounterMaxLength());
        } else {
            value=true;
            ilNilai.setError(null);
        }
        return value;
    }

    private boolean validateTglFrom() {
        boolean value;
        if (edTglFrom.getText().toString().isEmpty()){
            value=false;
            requestFocus(edTglFrom);
            Toast.makeText(MasterPromoItemActivity.this, "Tanggal awal harus diisi!", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    private boolean validateTglTo() {
        boolean value;
        if (edTglTo.getText().toString().isEmpty()){
            value=false;
            requestFocus(edTglTo);
            Toast.makeText(MasterPromoItemActivity.this, "Tanggal akhir harus diisi!", Toast.LENGTH_LONG).show();
        } else {
            value=true;
        }
        return value;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        if(requestCode == 2) {
            if(resultCode == RESULT_OK) {
                edKodeBrg.setText(data.getStringExtra("nama"));
                kodeBrg = data.getStringExtra("kode");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
