package com.example.aryaloka.Master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.aryaloka.Model.master.MasterKaryawanModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Transaksi.TransListActivity;
import com.example.aryaloka.Utilities.GlideApp;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.example.aryaloka.Utilities.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasterKaryawanItemActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private ProgressBar prb;
    private String userId, level, status;
    private ImageView imgBack, imgTgl, imgProfil;
    private TextInputEditText edNama, edKota, edTelp, edTglLahir;
    private TextInputLayout ilNama, ilKota, ilTelp;
    private Button btnSave, btnKeahlian;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private MasterKaryawanModel model;
    private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private Calendar dateAndTime = Calendar.getInstance();
    private Date tglLahir = null;
    private static final int REQUEST_CODE_GALLERY = 0013;
    private Uri selectedImage;
    private String hasilFoto = "N";
    private FirebaseStorage storage;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_karyawan_item);
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (i != null) {
            try {
                status = bundle.getString("Status");// ADD/EDIT
            } catch (Exception e) {
            }
        }
        prefUtil = new PrefUtil(this);
        mApiService = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try {
            shared = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        } catch (Exception e) {
            e.getMessage();
        }

        imgBack = (ImageView) findViewById(R.id.imgViewKaryBack);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewKaryNama);
        ilNama = (TextInputLayout) findViewById(R.id.input_layout_kary_nama);
        prb = (ProgressBar) findViewById(R.id.prbFotoProfileKary);
//        edKode = (TextInputEditText) findViewById(R.id.eAddViewKaryKode);
//        ilKode = (TextInputLayout) findViewById(R.id.input_layout_kary_kode);
        edTelp = (TextInputEditText) findViewById(R.id.eAddViewKaryTelp);
        ilTelp = (TextInputLayout) findViewById(R.id.input_layout_kary_telp);
        edKota = (TextInputEditText) findViewById(R.id.eAddViewKaryKota);
        ilKota = (TextInputLayout) findViewById(R.id.input_layout_kary_kota);
        edTglLahir = (TextInputEditText) findViewById(R.id.eAddViewKaryTgllahir);
        imgTgl = (ImageView) findViewById(R.id.imgKaryTglLahir);
        imgProfil = (ImageView) findViewById(R.id.imgFotoProfileKary);
        btnSave = (Button) findViewById(R.id.btnViewKarySave);
        btnKeahlian = (Button) findViewById(R.id.btnViewKaryKeahlian);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        if (status.equals("EDIT") || status.equals("VIEW")) {
            model = (MasterKaryawanModel) i.getSerializableExtra("model");
//            edKode.setEnabled(false);
            edNama.setText(model.getNamakary());
//            edKode.setText(model.getIdKaryawan());
            edTelp.setText(model.getTelp());
            edKota.setText(model.getKota());
            Date tgl = null;
            try {
                tgl = df2.parse(model.getTgllahir());
            } catch (Exception ex) {
            }
            edTglLahir.setText(sdf1.format(tgl));
            if(model.getPathFoto()!=null && !model.getPathFoto().equals("")){
                imgProfil.setBackgroundResource(0);
                prb.setVisibility(View.VISIBLE);
                StorageReference ref = storageReference.child("profile/"+model.getPathFoto().trim());
                Glide.with(this /* context */)
                        .load(ref)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                prb.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                prb.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(imgProfil);
            }
            btnKeahlian.setVisibility(View.VISIBLE);
        } else {
//            edKode.setEnabled(true);
            prb.setVisibility(View.GONE);
            GlideApp.with(this /* context */)
                    .load(R.drawable.galery)
                    .override(400, 400)
                    .fitCenter()
                    .into(imgProfil);
            btnKeahlian.setVisibility(View.GONE);
        }

        if(status.equals("VIEW")){
            btnSave.setVisibility(View.GONE);
            btnKeahlian.setVisibility(View.GONE);
            imgProfil.setEnabled(false);
            imgTgl.setVisibility(View.GONE);
        }else{
            btnSave.setVisibility(View.VISIBLE);
            btnKeahlian.setVisibility(View.VISIBLE);
            imgProfil.setEnabled(true);
            imgTgl.setVisibility(View.VISIBLE);
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        imgTgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(status.equals("VIEW")){
                    Toast.makeText(MasterKaryawanItemActivity.this, "Not Allowed", Toast.LENGTH_LONG).show();
                }else {
                    new DatePickerDialog(MasterKaryawanItemActivity.this, dFrom, dateAndTime.get(Calendar.YEAR), dateAndTime.get(Calendar.MONTH),
                            dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });

        imgProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status.equals("VIEW")){
                    Toast.makeText(MasterKaryawanItemActivity.this, "Not Allowed", Toast.LENGTH_LONG).show();
                }else {
                    openImage();
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateUserName(edNama.length()) && validateUserkota(edKota.length()) &&
                    validateUsertelp(edTelp.length()) && validateUsertgllahir(edTglLahir.length())) {
                    if(status.equals("VIEW")){
                        Toast.makeText(MasterKaryawanItemActivity.this, "Not Allowed", Toast.LENGTH_LONG).show();
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MasterKaryawanItemActivity.this);
                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Data akan diproses?")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (status.equals("ADD")) {
                                            model = new MasterKaryawanModel();
                                            //model.setIdKaryawan(edKode.getText().toString());
                                            model.setNamakary(edNama.getText().toString());
                                            model.setKota(edKota.getText().toString());
                                            model.setTelp(edTelp.getText().toString());
                                            model.setTgllahir(df2.format(tglLahir));
                                            insertData(model, userId, (hasilFoto.equals("Y") ? true : false));
                                        /*if(hasilFoto.equals("Y")) {
                                            model.setPathFoto(edKode.getText().toString() + ".jpg");
                                            uploadImage(model);
                                        }else {
                                            model.setPathFoto(null);
                                            insertData(model, userId);
                                        }*/
                                        } else if (status.equals("EDIT")) {
                                            updateData(model, userId);
                                        }
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                }
            }
        });

        btnKeahlian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status.equals("VIEW")){
                    Toast.makeText(MasterKaryawanItemActivity.this, "Not Allowed", Toast.LENGTH_LONG).show();
                }else {
                    if (status.equals("EDIT")) {
                        Intent i = new Intent(MasterKaryawanItemActivity.this, MasterKeahlianKaryActivity.class);
                        i.putExtra("model", model);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } else {
                        Toast.makeText(MasterKaryawanItemActivity.this, "Harap mengisi master karyawan terlebih dahulu", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void insertData(MasterKaryawanModel entity, String userId, boolean foto){
        pDialog.setMessage("Simpan Data ...\nHarap Tunggu");
        showDialog();
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        String tanggalNow = df.format(today);
        Integer year = cal.get(Calendar.YEAR);
        Integer month = cal.get(Calendar.MONTH)+1;
        mApiService.insertKaryawan(entity.getIdKaryawan(), entity.getNamakary(), entity.getTelp(), entity.getTgllahir(),
                entity.getKota(), year, month, foto, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    String kode = jsonRESULTS.getString("kodeKary");
                                    if(hasilFoto.equals("Y")) {
                                        model.setPathFoto(kode + ".jpg");
                                        uploadImage(model);
                                    }else{
                                        hideDialog();
                                    }
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MasterKaryawanItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterKaryawanItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterKaryawanItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MasterKaryawanItemActivity.this, "PROSES SIMPAN GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MasterKaryawanItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateData(MasterKaryawanModel entity, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.updateKaryawan(entity.getIdKaryawan(), entity.getNamakary(), entity.getTelp(), entity.getTgllahir(), entity.getKota(), tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MasterKaryawanItemActivity.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MasterKaryawanItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterKaryawanItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterKaryawanItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MasterKaryawanItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MasterKaryawanItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }


//    private boolean validateUserId(int length) {
//        boolean value;
//        if (edKode.getText().toString().isEmpty()){
//            value=false;
//            requestFocus(edKode);
//            ilKode.setError(getString(R.string.err_msg_user));
//        } else if (length > ilKode.getCounterMaxLength()) {
//            value=false;
//            ilKode.setError("Max character User ID length is " + ilKode.getCounterMaxLength());
//        }else {
//            value=true;
//            ilKode.setError(null);
//        }
//        return value;
//    }

    private boolean validateUserName(int length) {
        boolean value;
        if (edNama.getText().toString().isEmpty()){
            value=false;
            requestFocus(edNama);
            ilNama.setError(getString(R.string.err_msg_nama));
        } else if (length > ilNama.getCounterMaxLength()) {
            value=false;
            ilNama.setError("Max character User ID length is " + ilNama.getCounterMaxLength());
        }else {
            value=true;
            ilNama.setError(null);
        }
        return value;
    }

    private boolean validateUsertelp(int length) {
        boolean value;
        if (edTelp.getText().toString().isEmpty()){
            value=false;
            requestFocus(edTelp);
            ilTelp.setError(getString(R.string.err_msg_telp));
        } else if (length > ilTelp.getCounterMaxLength()) {
            value=false;
            ilTelp.setError("Max character User ID length is " + ilTelp.getCounterMaxLength());
        }else {
            value=true;
            ilTelp.setError(null);
        }
        return value;
    }

    private boolean validateUserkota(int length) {
        boolean value;
        if (edKota.getText().toString().isEmpty()){
            value=false;
            requestFocus(edKota);
            ilKota.setError(getString(R.string.err_msg_kota));
        } else if (length > ilKota.getCounterMaxLength()) {
            value=false;
            ilKota.setError("Max character User ID length is " + ilKota.getCounterMaxLength());
        }else {
            value=true;
            ilKota.setError(null);
        }
        return value;
    }

    private boolean validateUsertgllahir(int length) {
        boolean value;
        if (edTglLahir.getText().toString().isEmpty()){
            value=false;
            requestFocus(edTglLahir);
        } else {
            value=true;
        }
        return value;
    }

    private void uploadImage(MasterKaryawanModel model) {
        if (selectedImage != null) {
            pDialog.setMessage("Uploading Image...");
            showDialog();
            StorageReference ref = storageReference.child("profile/"+ model.getPathFoto().trim());

            ref.putFile(selectedImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {

                        @Override
                        public void onSuccess(
                                UploadTask.TaskSnapshot taskSnapshot){
                            //insertData(model, userId);
                            hideDialog();
                            Toast.makeText(MasterKaryawanItemActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK,returnIntent);
                            finish();
                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure( Exception e){
                            hideDialog();
                            Toast.makeText(MasterKaryawanItemActivity.this,"Failed " + e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot){
                                }
                            });
        }
    }

    private void openImage() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_CODE_GALLERY);
    }

    private DatePickerDialog.OnDateSetListener dFrom = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, month);
            dateAndTime.set(Calendar.DAY_OF_MONTH, day);
            updatelabelFrom();
        }
    };

    private void updatelabelFrom() {
        edTglLahir.setText(sdf1.format(dateAndTime.getTime()));
        tglLahir = dateAndTime.getTime();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        Utils.freeMemory();
        super.onDestroy();
        Utils.trimCache(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                hasilFoto = "Y";
                selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                imgProfil.setBackgroundResource(0);
                Glide.with(this)
                        .load(Uri.parse("file:///"+picturePath))
                        .into(imgProfil);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}