package com.example.aryaloka.Master;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.aryaloka.R;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasterUserItemActivity extends AppCompatActivity {

    private PrefUtil prefUtil;
    private SharedPreferences shared;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private String userId, level, status, nama, kode, email, hp, pasw, stat;
    private ImageView imgBack;
    private TextInputEditText edNama, edKode, edEmail, edHp, edPasw;
    private TextInputLayout ilNama, ilKode, ilHp, ilEmail, ilPasw;
    private RadioButton rbAktif, rbNon;
    private Button btnSave;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_user_item_activity);
        Bundle i = getIntent().getExtras();
        if (i != null){
            try {
                status = i.getString("Status");// ADD/EDIT
                kode = i.getString("kode");
                nama = i.getString("nama");
                email = i.getString("email");
                hp = i.getString("telp");
                pasw = i.getString("pasw");
                stat = i.getString("stat");
            } catch (Exception e) {}
        }
        prefUtil            = new PrefUtil(this);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        try{
            shared  = prefUtil.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}

        imgBack = (ImageView)findViewById(R.id.imgViewUserBack);
        edKode = (TextInputEditText) findViewById(R.id.eAddViewUserKode);
        ilKode = (TextInputLayout) findViewById(R.id.input_layout_user_kode);
        edNama = (TextInputEditText) findViewById(R.id.eAddViewUserNama);
        ilNama = (TextInputLayout) findViewById(R.id.input_layout_user_nama);
        edHp = (TextInputEditText) findViewById(R.id.eAddViewUserHp);
        ilHp = (TextInputLayout) findViewById(R.id.input_layout_user_hp);
        edEmail = (TextInputEditText) findViewById(R.id.eAddViewUserEmail);
        ilEmail = (TextInputLayout) findViewById(R.id.input_layout_user_email);
        edPasw = (TextInputEditText) findViewById(R.id.eAddViewUserPasw);
        ilPasw = (TextInputLayout) findViewById(R.id.input_layout_user_pasw);
        rbAktif = (RadioButton)findViewById(R.id.rbUserViewAktif);
        rbNon = (RadioButton)findViewById(R.id.rbUserViewNonAktif);
        btnSave = (Button) findViewById(R.id.btnViewUserSave);

        if(status.equals("EDIT")){
            edNama.setText(nama);
            edKode.setText(kode);
            edEmail.setText(email);
            edHp.setText(hp);
            edPasw.setText(pasw);
            if(stat.equals("A")){
                rbAktif.setChecked(true);
                rbNon.setChecked(false);
            }else{
                rbAktif.setChecked(false);
                rbNon.setChecked(true);
            }
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validatePasw(edPasw.length()) ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MasterUserItemActivity.this);
                    builder.setTitle("Konfirmasi");
                    builder.setMessage("Data akan diproses?")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if(status.equals("EDIT")){
                                        String stat = "";
                                        if(rbAktif.isChecked()) stat = "A";
                                        else stat = "D";
                                        updateData(edKode.getText().toString(), edPasw.getText().toString(), stat, userId);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });
    }

    private void updateData(String kode, String pasw, String stat, String userId){
        pDialog.setMessage("Update Data ...\nHarap Tunggu");
        showDialog();
        Date today = Calendar.getInstance().getTime();
        String tanggalNow =df.format(today);
        mApiService.updateUser(kode, pasw, stat, tanggalNow, userId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("value").equals("false")){
                                    hideDialog();
                                    Toast.makeText(MasterUserItemActivity.this, "DATA BERHASIL DIPERBARUI", Toast.LENGTH_LONG).show();

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                } else {
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(MasterUserItemActivity.this, error_message, Toast.LENGTH_LONG).show();
                                    hideDialog();
                                }
                            } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterUserItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                hideDialog();
                                e.printStackTrace();
                                Toast.makeText(MasterUserItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hideDialog();
                            Toast.makeText(MasterUserItemActivity.this, "PROSES UPDATE GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideDialog();
                        Toast.makeText(MasterUserItemActivity.this, "Koneksi Internet Bermasalah", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private boolean validatePasw(int length) {
        boolean value=true;
        int minValue = 6;
        if (edPasw.getText().toString().trim().isEmpty()) {
            value=false;
            requestFocus(edPasw);
            ilPasw.setError(getString(R.string.err_msg_sandi));
        } else if (length > ilPasw.getCounterMaxLength()) {
            value=false;
            ilPasw.setError("Max character password length is " + ilPasw.getCounterMaxLength());
        } else if (length < minValue) {
            value=false;
            ilPasw.setError("Min character password length is 6" );
        } else{
            value=true;
            ilPasw.setError(null);}
        return value;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
