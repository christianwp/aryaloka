package com.example.aryaloka.Master;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.example.aryaloka.Adapter.Master.AdpMsPromo;
import com.example.aryaloka.Model.master.MasterPromoModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Utilities.AppController;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;

public class MasterPromoActivity extends AppCompatActivity {

    private ImageView ImgAdd, imgBack;
    private AdpMsPromo adapter;
    private ListView lsvupload;
    private ArrayList<MasterPromoModel> columnlist= new ArrayList<MasterPromoModel>();
    private TextView tvstatus;
    private ProgressBar prbstatus;
    private String getUpload	="promo_list.php";
    private PrefUtil pref;
    private SharedPreferences shared;
    private String level, userId;
    private int RESULT_DEPT = 9;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_promo);
        pref = new PrefUtil(this);
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            level = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){e.getMessage();}
        ImgAdd		= (ImageView)findViewById(R.id.imgListMasterPromoAdd);
        imgBack		= (ImageView)findViewById(R.id.imgMasterPromoBack);
        lsvupload	= (ListView)findViewById(R.id.listMasterPromo);
        tvstatus	= (TextView)findViewById(R.id.txtListMasterPromoStatus);
        prbstatus	= (ProgressBar)findViewById(R.id.prbListMasterPromoStatus);

        adapter		= new AdpMsPromo(MasterPromoActivity.this, R.layout.adp_ms_promo, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
        if(level.equals("A")){
            getDataUpload(Link.BASE_URL_API+getUpload, level);
        }else{
            getDataUpload(Link.BASE_URL_API+getUpload, level);
        }

        if(level.equals("A")){
            ImgAdd.setVisibility(View.VISIBLE);
        }else{
            ImgAdd.setVisibility(View.INVISIBLE);
            //Toast.makeText(MasterTipeBarang.this,"User Level anda tidak berhak mengakses menu ini!", Toast.LENGTH_LONG).show();
            //finish();
        }

        ImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(level.equals("A")){
                    Intent i  = new Intent(MasterPromoActivity.this, MasterPromoItemActivity.class);
                    i.putExtra("Status", "ADD");
                    startActivityForResult(i, 9);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getDataUpload(String Url, String levelku){
        tvstatus.setVisibility(View.GONE);
        prbstatus.setVisibility(View.VISIBLE);
        JsonObjectRequest jsonget = new JsonObjectRequest(Request.Method.GET, Url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int sucses= response.getInt("success");
                            if (sucses==1){
                                tvstatus.setVisibility(View.GONE);
                                prbstatus.setVisibility(View.GONE);
                                adapter.clear();
                                JSONArray JsonArray = response.getJSONArray("uploade");
                                for (int i = 0; i < JsonArray.length(); i++) {
                                    JSONObject object = JsonArray.getJSONObject(i);
                                    MasterPromoModel colums 	= new MasterPromoModel();
                                    colums.setKodePromo(object.getString("c_kodepromo"));
                                    colums.setNamaPromo(object.getString("vc_namapromo"));
                                    colums.setKodeBarang(object.getString("c_kodebarang"));
                                    colums.setNamaBarang(object.getString("vc_namabarang"));
                                    colums.setNilai(new BigDecimal(object.getDouble("n_nilaipromo")));
                                    colums.setTglFrom(object.getString("dt_from"));
                                    colums.setTglTo(object.getString("dt_to"));
                                    colums.setStatus(object.getString("status_disc"));
                                    columnlist.add(colums);
                                }
                            }else{
                                columnlist = new ArrayList<MasterPromoModel>();
                                tvstatus.setVisibility(View.VISIBLE);
                                tvstatus.setText("Tidak Ada Data");
                                prbstatus.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter.notifyDataSetChanged();
                        adapter		= new AdpMsPromo(MasterPromoActivity.this, R.layout.adp_ms_promo, columnlist, level, userId);
                        lsvupload.setAdapter(adapter);
                        //lsvupload.invalidate();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check Koneksi Internet Anda");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof AuthFailureError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("AuthFailureError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ServerError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ServerError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof NetworkError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check NetworkError");
                    prbstatus.setVisibility(View.GONE);
                } else if (error instanceof ParseError) {
                    tvstatus.setVisibility(View.VISIBLE);
                    tvstatus.setText("Check ParseError");
                    prbstatus.setVisibility(View.GONE);
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("level", levelku);
                return params;
            }
            @Override
            public java.util.Map<String, String> getHeaders() throws AuthFailureError {
                java.util.Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };
        AppController.getInstance().getRequestQueue().getCache().invalidate(Url, true);
        AppController.getInstance().addToRequestQueue(jsonget);
    }

    @Override
    protected void onResume() {
        super.onResume();
        columnlist = new ArrayList<MasterPromoModel>();
        adapter		= new AdpMsPromo(MasterPromoActivity.this, R.layout.adp_ms_promo, columnlist, level, userId);
        lsvupload.setAdapter(adapter);
        if(level.equals("A")){
            getDataUpload(Link.BASE_URL_API+getUpload, level);
        }else{
            getDataUpload(Link.BASE_URL_API+getUpload, level);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RESULT_DEPT) {
            if(resultCode == RESULT_OK) {
                columnlist = new ArrayList<MasterPromoModel>();
                adapter		= new AdpMsPromo(MasterPromoActivity.this, R.layout.adp_ms_promo, columnlist, level, userId);
                lsvupload.setAdapter(adapter);
                if(level.equals("A")){
                    getDataUpload(Link.BASE_URL_API+getUpload, level);
                }else{
                    getDataUpload(Link.BASE_URL_API+getUpload, level);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
