package com.example.aryaloka;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.aryaloka.Master.MasterKaryawanActivity;
import com.example.aryaloka.Master.MasterPromoActivity;
import com.example.aryaloka.Master.MasterTipeBarang;
import com.example.aryaloka.Master.MasterUserActivity;

import androidx.fragment.app.Fragment;

public class FragmentMaster extends Fragment {

    private View vupload;
    private LinearLayout linMsBrg, linMsPromo, linMsKary, linMsPel, linMs1, linMs2;
    private String userId, username, telp, email, status, paswd;

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle	 = this.getArguments();
        if (bundle!=null){
            userId	= bundle.getString("userId");
            username	= bundle.getString("username");
            telp	= bundle.getString("telp");
            email	= bundle.getString("email");
            status	= bundle.getString("status");
        }
        vupload     = inflater.inflate(R.layout.frg_master, container,false);
        linMsBrg = (LinearLayout)vupload.findViewById(R.id.linMsBarang);
        linMsPromo = (LinearLayout)vupload.findViewById(R.id.linMsPromo);
        linMsKary = (LinearLayout)vupload.findViewById(R.id.linMsKaryawan);
        linMsPel = (LinearLayout)vupload.findViewById(R.id.linMsPelanggan);
        linMs1 = (LinearLayout)vupload.findViewById(R.id.linMs1);
        linMs2 = (LinearLayout)vupload.findViewById(R.id.linMs2);

        if(status.equals("A")){
            linMs1.setVisibility(View.VISIBLE);
            linMsBrg.setVisibility(View.VISIBLE);
            linMsKary.setVisibility(View.VISIBLE);
            linMs2.setVisibility(View.VISIBLE);
            linMsPromo.setVisibility(View.VISIBLE);
            linMsPel.setVisibility(View.VISIBLE);
        }else if(status.equals("C")){
            linMs1.setVisibility(View.GONE);
            linMsBrg.setVisibility(View.GONE);
            linMsKary.setVisibility(View.GONE);
            linMs2.setVisibility(View.VISIBLE);
            linMsPel.setVisibility(View.GONE);
            linMsPromo.setVisibility(View.VISIBLE);
        }else if(status.equals("K")){
            linMs1.setVisibility(View.GONE);
            linMsBrg.setVisibility(View.GONE);
            linMsKary.setVisibility(View.GONE);
            linMs2.setVisibility(View.VISIBLE);
            linMsPel.setVisibility(View.GONE);
            linMsPromo.setVisibility(View.VISIBLE);
        }

        linMsBrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MasterTipeBarang.class));
            }
        });

        linMsPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MasterPromoActivity.class));
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        linMsKary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MasterKaryawanActivity.class));
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        linMsPel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), MasterUserActivity.class));
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        return vupload;
    }
}
