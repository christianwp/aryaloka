package com.example.aryaloka.Utilities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefUtil {

    private Activity activity;
    public static final String ID = "ID";
    public static final String NAME = "NAME";
    public static final String STATUS = "STATUS";// C/A
    public static final String PATH = "PATH";
    public static final String PASW = "PASW";
    public static final String TELP = "TELP";
    public static final String EMAIL = "EMAIL";

    public PrefUtil(Activity activity) {
        this.activity = activity;
    }

    public void clear() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply(); // This line is IMPORTANT !!!
    }

    public void saveUserInfo(String id, String name, String status, String path, String pasw, String telp,
                             String email){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ID, id);
        editor.putString(NAME, name);
        editor.putString(STATUS, status);
        editor.putString(PATH, path);
        editor.putString(PASW, pasw);
        editor.putString(TELP, telp);
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public SharedPreferences getUserInfo(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        //Log.d("MyApp", "Name : "+prefs.getString("fb_name",null)+"\nEmail : "+prefs.getString("fb_email",null));
        return prefs;
    }
}
