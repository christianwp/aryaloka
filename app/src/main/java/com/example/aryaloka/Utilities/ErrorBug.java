package com.example.aryaloka.Utilities;

import android.os.Bundle;
import android.widget.TextView;

import com.example.aryaloka.R;

import androidx.appcompat.app.AppCompatActivity;

public class ErrorBug extends AppCompatActivity {
    private TextView tvError;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_bug);

        tvError     = (TextView)findViewById(R.id.TvErorr);
        tvError.setText(getIntent().getStringExtra("error"));

    }
}
