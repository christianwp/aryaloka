package com.example.aryaloka.Utilities;

import com.example.aryaloka.Service.BaseApiService;

public class Link {

    public static final String BASE_URL_API = "http://softchrist.com/aryaloka/php/";
    public static final String BASE_URL_IMAGE_PROFILE = "http://softchrist.com/aryaloka/profile/";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }

    public static BaseApiService getImgProfileService(){
        return RetrofitClient.getClient(BASE_URL_IMAGE_PROFILE).create(BaseApiService.class);
    }
}
