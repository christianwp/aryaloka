package com.example.aryaloka.Utilities;

import com.example.aryaloka.List.Model.ListPromoModel;
import com.example.aryaloka.List.Model.ListTipeBarangModel;
import com.example.aryaloka.List.View.ListTipeBarang;

public class JSONResponse {

    private ListTipeBarangModel[] listTipeBarang;
    private ListPromoModel[] listPromo;

    public ListTipeBarangModel[] getListTipeBarang() {
        return listTipeBarang;
    }

    public void setListTipeBarang(ListTipeBarangModel[] listTipeBarang) {
        this.listTipeBarang = listTipeBarang;
    }

    public ListPromoModel[] getListPromo() {
        return listPromo;
    }

    public void setListPromo(ListPromoModel[] listPromo) {
        this.listPromo = listPromo;
    }
}
