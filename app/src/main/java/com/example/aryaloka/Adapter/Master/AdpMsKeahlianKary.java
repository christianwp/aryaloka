package com.example.aryaloka.Adapter.Master;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.example.aryaloka.Master.MasterKeahlianKaryItemActivity;
import com.example.aryaloka.Model.master.MasterKeahlianKaryModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Utilities.AppController;
import com.example.aryaloka.Utilities.Link;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;

public class AdpMsKeahlianKary extends ArrayAdapter<MasterKeahlianKaryModel> {

    private List<MasterKeahlianKaryModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private ViewHolder holder;
    private Context context;
    private AlertDialog alert;
    private String FileDeteleted = "keahlian_delete.php";
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String levelku, userIdku;

    public AdpMsKeahlianKary(Context context, int resource, List<MasterKeahlianKaryModel> objects, String level,
                         String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.ImgDelete	=	 (ImageView)v.findViewById(R.id.imgColMasterKaryAhliDelete);
            holder.ImgEdit		=	 (ImageView)v.findViewById(R.id.imgColMasterAhliKaryEdit);
            holder.TvNama	    =	 (TextView)v.findViewById(R.id.txtColMasterAhliKaryawanNama);
            holder.tvId	    =	 (TextView)v.findViewById(R.id.txtColMasterAhliKaryawanKode);
            holder.tvNmBrg	    =	 (TextView)v.findViewById(R.id.txtColMasterAhliKaryawanNmBrg);
            holder.tvNilai	    =	 (TextView)v.findViewById(R.id.txtColMasterAhliKaryawanNilai);
            holder.tvStatus	    =	 (TextView)v.findViewById(R.id.txtColMasterAhliKaryawanStatus);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }

        if(levelku.equals("A")){
            holder.ImgDelete.setVisibility(View.VISIBLE);
            holder.ImgEdit.setVisibility(View.VISIBLE);
        }else{
            holder.ImgDelete.setVisibility(View.GONE);
            holder.ImgEdit.setVisibility(View.GONE);
        }

        holder.ImgDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder msMaintance = new AlertDialog.Builder(getContext());
                msMaintance.setCancelable(false);
                msMaintance.setMessage("Yakin akan dihapus? ");
                msMaintance.setNegativeButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String kode = columnslist.get(position).getKodeKary();
                        String kodeBrg = columnslist.get(position).getKodeKary();
                        deletedData(Link.BASE_URL_API + FileDeteleted, position, kode, kodeBrg, userIdku);
                    }
                });

                msMaintance.setPositiveButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                });
                alert	=msMaintance.create();
                alert.show();
            }
        });

        holder.ImgEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MasterKeahlianKaryItemActivity.class);
                i.putExtra("Status", "EDIT");
                i.putExtra("kodeKary", columnslist.get(position).getKodeKary());
                i.putExtra("kodeBrg", columnslist.get(position).getKodeBarang());
                i.putExtra("namaBrg", columnslist.get(position).getNamaBarang());
                i.putExtra("nilai", String.valueOf(columnslist.get(position).getNilai()));
                ((Activity) context).startActivityForResult(i, 10);
            }
        });

        holder.TvNama.setText("Nama: "+columnslist.get(position).getNamaKary());
        holder.tvId.setText("ID: "+columnslist.get(position).getKodeKary());
        holder.tvNmBrg.setText("Barang: "+columnslist.get(position).getNamaBarang());
        holder.tvNilai.setText("Skor Nilai: "+String.valueOf(columnslist.get(position).getNilai()));
        holder.tvStatus.setText(columnslist.get(position).getCstat().equals("A")?"Active":"Deactive");
        return v;
    }

    private void deletedData(String save, final int position, final String kodeKary, String kodeBrg, final String userId){
        Date today = Calendar.getInstance().getTime();
        final String tanggalNow =dfSave.format(today);
        StringRequest register = new StringRequest(Request.Method.POST, save,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonrespon = new JSONObject(response);
                            int Sucsess = jsonrespon.getInt("success");
                            String message = jsonrespon.getString("message");
                            if (Sucsess ==1 ){
                                columnslist.remove(position);
                                notifyDataSetChanged();
                                Toast.makeText(getContext(), "DATA BERHASIL DI HAPUS", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(getContext(), "HAPUS DATA GAGAL", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck Koneksi Internet Anda", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nAuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Toast.makeText(getContext(), "HAPUS DATA GAGAL.\nCheck ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            protected java.util.Map<String, String> getParams() {
                java.util.Map<String, String> params = new HashMap<String, String>();
                params.put("kodeKary", kodeKary);
                params.put("kodeBarang", kodeBrg);
                params.put("kodeUser", userId);
                params.put("tglNow", tanggalNow);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(register);
    }

    static class ViewHolder{
        private ImageView ImgDelete;
        private ImageView ImgEdit;
        private TextView tvId;
        private TextView TvNama;
        private TextView tvNmBrg;
        private TextView tvNilai;
        private TextView tvStatus;
    }
}
