package com.example.aryaloka.Adapter.Transaksi;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.aryaloka.Model.transaksi.ChattingModel;
import com.example.aryaloka.R;
import com.example.aryaloka.Utilities.Utils;
import com.google.firebase.storage.StorageReference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class AdpChatting extends ArrayAdapter<ChattingModel> {

    private List<ChattingModel> columnslist; //diambil Dari JColum Tempat Mendaftar String Data
    private LayoutInflater vi;
    private int Resource;
    private ViewHolder holder;
    private Context context;
    private StorageReference storageReference;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String id;

    public AdpChatting(Context context, int resource, List<ChattingModel> objects, StorageReference store, String userid) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        storageReference = store;
        id = userid;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.ImgUser				= 	 (ImageView)v.findViewById(R.id.imgColChattingtUser);
            holder.TvUser				= 	 (TextView)v.findViewById(R.id.tvColChattingNameUser);
            holder.TvDate				=	 (TextView)v.findViewById(R.id.tvColChattingTime);
            holder.TvDescription		=	 (TextView)v.findViewById(R.id.tvColChattingMessage);
            holder.progress             =   (ProgressBar)v.findViewById(R.id.prbColChattingDetail);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }

        StorageReference ref = storageReference.child("profile/"+columnslist.get(position).getKodeUser().trim()+".jpg");
        Glide.with(this.context /* context */)
                .load(ref)
                .transition(DrawableTransitionOptions.withCrossFade())
                .apply(new RequestOptions().override(240,120))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progress.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.ImgUser);

        Utils.currentTime(columnslist.get(position).getTanggal(), holder.TvDate);
        holder.TvDescription.setText(columnslist.get(position).getText());
        holder.TvUser.setText(columnslist.get(position).getNamaUser());
        return v;
    }

    static class ViewHolder{
        private ImageView ImgUser;
        private TextView TvDescription;
        private TextView TvUser;
        private TextView TvDate;
        private ProgressBar progress;
    }
}