package com.example.aryaloka.Adapter.Transaksi;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.aryaloka.Adapter.Master.AdpMsPromo;
import com.example.aryaloka.Model.transaksi.TrHistCustModel;
import com.example.aryaloka.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdpTrHistCust extends ArrayAdapter<TrHistCustModel> {

    private List<TrHistCustModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private ViewHolder holder;
    private Context context;
    private AlertDialog alert;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private String levelku, userIdku;

    public AdpTrHistCust(Context context, int resource, List<TrHistCustModel> objects, String level,
                         String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNoTrans	    =	 (TextView)v.findViewById(R.id.txtColHistCustNoTrans);
            holder.TvTglTrans	    =	 (TextView)v.findViewById(R.id.txtColHistCustTglTrans);
            holder.TvStatusTrans	=	 (TextView)v.findViewById(R.id.txtColHistCustStatusProgress);
            holder.TvNmBarang	    =	 (TextView)v.findViewById(R.id.txtColHistCustNmbarang);
            holder.TvTeknisi	    =	 (TextView)v.findViewById(R.id.txtColHistCustNamaTeknisi);
            holder.TvTglSelesai	    =	 (TextView)v.findViewById(R.id.txtColHistCustTglSelesai);
            holder.TvTglGaransi	    =	 (TextView)v.findViewById(R.id.txtColHistCustTglGaransi);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }
        Date tglTrans = Calendar.getInstance().getTime();
        Date tglSelesai = Calendar.getInstance().getTime();
        Date tglGaransi = Calendar.getInstance().getTime();
        try{
            tglTrans = dfSave.parse(columnslist.get(position).getTglTrans());
            if(columnslist.get(position).getTglSelesai()!=null)
                tglSelesai = dfSave.parse(columnslist.get(position).getTglSelesai());
            if(columnslist.get(position).getTglBatasGaransi()!=null)
                tglGaransi = dfSave.parse(columnslist.get(position).getTglBatasGaransi());
        }catch (Exception ex){}
        String status = "";
        if(columnslist.get(position).getKodeUserTeknisi()!=null && columnslist.get(position).getTglAmbil()!=null &&
                columnslist.get(position).getTglSelesai()!=null && columnslist.get(position).getTglAntar()!=null)
            status = "Proses Selesai";
        else if(columnslist.get(position).getKodeUserTeknisi()!=null && columnslist.get(position).getTglAmbil()!=null &&
                columnslist.get(position).getTglSelesai()!=null && columnslist.get(position).getTglAntar()==null)
            status = "Teknisi akan menuju ke lokasi untuk mengantar barang";
        else if(columnslist.get(position).getKodeUserTeknisi()!=null && columnslist.get(position).getTglAmbil()!=null &&
                columnslist.get(position).getTglSelesai()==null)
            status = "Proses pengerjaan teknisi";
        else if(columnslist.get(position).getKodeUserTeknisi()!=null && columnslist.get(position).getTglAmbil()==null &&
                columnslist.get(position).getTglSelesai()==null)
            status = "Teknisi menuju lokasi untuk menjemput barang";
        else if(columnslist.get(position).getKodeUserTeknisi()==null)
            status = "Menunggu Teknisi";
        else
            status = "Invalid Status";

        holder.TvNoTrans.setText("No Transaksi: "+ columnslist.get(position).getNoTrans());
        holder.TvTglTrans.setText("Tgl Transaksi: "+ sdf1.format(tglTrans));
        holder.TvStatusTrans.setText("Status: "+ status);
        holder.TvNmBarang.setText("Barang: "+ columnslist.get(position).getNamaTipeBarang());
        holder.TvTeknisi.setText("Teknisi: "+ (columnslist.get(position).getNamaUserTeknisi()==null?"-":columnslist.get(position).getNamaUserTeknisi()));
        holder.TvTglSelesai.setText("Tanggal Selesai: "+ (columnslist.get(position).getTglSelesai()==null?"-":sdf1.format(tglSelesai)));
        holder.TvTglGaransi.setText("Garansi s/d tanggal "+ (columnslist.get(position).getTglBatasGaransi()==null?"-":sdf1.format(tglGaransi)));
        return v;
    }

    static class ViewHolder{
        private TextView TvNoTrans;
        private TextView TvTglTrans;
        private TextView TvStatusTrans;
        private TextView TvNmBarang;
        private TextView TvTeknisi;
        private TextView TvTglSelesai;
        private TextView TvTglGaransi;
    }
}
