package com.example.aryaloka.Adapter.Master;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aryaloka.Master.MasterUserItemActivity;
import com.example.aryaloka.Model.master.MasterUserModel;
import com.example.aryaloka.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class AdpMsUser extends ArrayAdapter<MasterUserModel> {

    private List<MasterUserModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private ViewHolder holder;
    private Context context;
    private String FileDeteleted = "user_delete.php";
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String levelku, userIdku;

    public AdpMsUser(Context context, int resource, List<MasterUserModel> objects, String level,
                          String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.ImgEdit		=	 (ImageView)v.findViewById(R.id.imgColMasterUserEdit);
            holder.TvKode	    =	 (TextView)v.findViewById(R.id.txtColMasterUserKode);
            holder.TvNama	    =	 (TextView)v.findViewById(R.id.txtColMasterUserNama);
            holder.TvEmail	    =	 (TextView)v.findViewById(R.id.txtColMasterUserEmail);
            holder.TvTelp	    =	 (TextView)v.findViewById(R.id.txtColMasterUserNoHp);
            holder.TvStat	    =	 (TextView)v.findViewById(R.id.txtColMasterUserStat);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }

        if(levelku.equals("A")){
            holder.ImgEdit.setVisibility(View.VISIBLE);
        }else{
            holder.ImgEdit.setVisibility(View.GONE);
        }

        holder.ImgEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MasterUserItemActivity.class);
                i.putExtra("Status", "EDIT");
                i.putExtra("kode", columnslist.get(position).getKodeUser());
                i.putExtra("nama", columnslist.get(position).getNamaUser());
                i.putExtra("telp", columnslist.get(position).getNoTelp());
                i.putExtra("email", columnslist.get(position).getEmail());
                i.putExtra("pasw", columnslist.get(position).getPasswoed());
                i.putExtra("stat", columnslist.get(position).getStat());
                ((Activity) context).startActivityForResult(i, 9);
            }
        });

        holder.TvKode.setText("ID: "+columnslist.get(position).getKodeUser());
        holder.TvNama.setText("Nama: "+columnslist.get(position).getNamaUser());
        holder.TvTelp.setText("Hp: "+columnslist.get(position).getNoTelp());
        holder.TvEmail.setText("Email: "+columnslist.get(position).getEmail());
        holder.TvStat.setText("Status: "+ (columnslist.get(position).getStat().equals("A")?"Aktif":"Tidak Aktif"));
        return v;
    }

    static class ViewHolder{
        private ImageView ImgEdit;
        private TextView TvKode;
        private TextView TvNama;
        private TextView TvTelp;
        private TextView TvEmail;
        private TextView TvStat;
    }
}
