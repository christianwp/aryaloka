package com.example.aryaloka.Adapter.Transaksi;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.aryaloka.Model.transaksi.TrUlasanTeknisiModel;
import com.example.aryaloka.R;
import com.willy.ratingbar.ScaleRatingBar;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class AdpUlasanTeknisi extends ArrayAdapter<TrUlasanTeknisiModel> {

    private List<TrUlasanTeknisiModel> columnslist;
    private LayoutInflater vi;
    private int Resource;
    private ViewHolder holder;
    private Context context;
    private AlertDialog alert;
    private DateFormat dfSave = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    private String levelku, userIdku;

    public AdpUlasanTeknisi(Context context, int resource, List<TrUlasanTeknisiModel> objects, String level,
                            String userId) {
        super(context, resource,  objects);
        this.context = context;
        vi	=	(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource		= resource;
        columnslist		= objects;
        levelku = level;
        userIdku = userId;
    }

    @Override
    public View getView (final int position, View convertView, final ViewGroup parent){
        View v	=	convertView;
        if (v == null){
            holder	=	new ViewHolder();
            v= vi.inflate(Resource, null);
            holder.TvNama	    =	 (TextView)v.findViewById(R.id.tvColUlasanTeknisiNamaUser);
            holder.TvNilai	    =	 (TextView)v.findViewById(R.id.tvColUlasanTeknisiRating);
            holder.TvUlasan	=	 (TextView)v.findViewById(R.id.tvColUlasanTeknisiText);
            holder.rating	    =	 (ScaleRatingBar) v.findViewById(R.id.rtColUlasanTeknisiRating);
            v.setTag(holder);
        }else{
            holder 	= (ViewHolder)v.getTag();
        }
        holder.TvNama.setText(columnslist.get(position).getNamaCust());
        holder.TvNilai.setText(String.valueOf(new BigDecimal(columnslist.get(position).getBintang()).setScale(1)));
        holder.TvUlasan.setText(columnslist.get(position).getText());
        holder.rating.setClickable(false);
        holder.rating.setScrollable(false);
        holder.rating.setRating(new BigDecimal(columnslist.get(position).getBintang()).floatValue());
        return v;
    }

    static class ViewHolder{
        private TextView TvNama;
        private TextView TvNilai;
        private TextView TvUlasan;
        private ScaleRatingBar rating;
    }
}