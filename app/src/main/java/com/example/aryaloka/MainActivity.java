package com.example.aryaloka;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aryaloka.Master.MasterKaryawanActivity;
import com.example.aryaloka.Master.MasterPromoActivity;
import com.example.aryaloka.Master.MasterTipeBarang;
import com.example.aryaloka.Master.MasterUserActivity;
import com.example.aryaloka.Service.BaseApiService;
import com.example.aryaloka.Transaksi.TransHistCustActivity;
import com.example.aryaloka.Transaksi.TransHistCustItemActivity;
import com.example.aryaloka.Transaksi.TransHistTeknisiActivity;
import com.example.aryaloka.Utilities.Link;
import com.example.aryaloka.Utilities.PrefUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private NavigationView navigationView;
    private PrefUtil pref;
    private SharedPreferences shared;
    private String userId, username, telp, email, status, paswd;
    private BaseApiService mApiService;
    private ProgressDialog pDialog;
    private DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
    private DrawerLayout drawer;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView txtId, txtNama;
    private LocationManager locationManager ;
    private static final String TAG = "MainActivity";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(userId);
            pref.clear();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        pref = new PrefUtil(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        mApiService         = Link.getAPIService();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        Intent i = getIntent();
        try{
            shared  = pref.getUserInfo();
            userId = shared.getString(PrefUtil.ID, null);
            username = shared.getString(PrefUtil.NAME, null);
            telp = shared.getString(PrefUtil.TELP, null);
            email = shared.getString(PrefUtil.EMAIL, null);
            status = shared.getString(PrefUtil.STATUS, null);
        }catch (Exception e){
            e.getMessage();
            //Crashlytics.logException(new Exception(e.getMessage()));
        }
        if(getIntent().getExtras()!=null){
            for(String key : getIntent().getExtras().keySet()){
                String value = getIntent().getExtras().getString(key);
                Log.d("TAG", "KEY : " + key + "Value : " + value);
            }
        }
        FirebaseApp.initializeApp(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        FirebaseMessaging.getInstance().subscribeToTopic(userId)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                String msg = "";
                if (!task.isSuccessful()) {
                    msg = "Koneksi Notifikasi Gagal:("+userId+")";
                }
                Log.d(TAG, msg);
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        //String token = FirebaseInstanceId.getInstance().getToken();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Aryaloka App");
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        txtId = (TextView)header.findViewById(R.id.txtNavHeaderIDUser);
        txtNama = (TextView)header.findViewById(R.id.txtNavHeaderNamaUser);
        txtId.setText("ID User: "+userId);
        txtNama.setText("Nama: "+username);

        if(status.equals("A")){
            menuAdmin();
        }else if(status.equals("C")){
            menuCust();
        }else if(status.equals("K")){
            menuKary();
        }
    }

    private void menuCust(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.msbarang).setVisible(false);
        nav_Menu.findItem(R.id.mskaryawan).setVisible(false);
        nav_Menu.findItem(R.id.mspelanggan).setVisible(false);
        nav_Menu.findItem(R.id.mspromo).setVisible(true);
        nav_Menu.findItem(R.id.trservis).setVisible(true);
        nav_Menu.findItem(R.id.trhistory).setVisible(true);
        nav_Menu.findItem(R.id.logout).setVisible(true);
    }

    private void menuAdmin(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.msbarang).setVisible(true);
        nav_Menu.findItem(R.id.mskaryawan).setVisible(true);
        nav_Menu.findItem(R.id.mspelanggan).setVisible(true);
        nav_Menu.findItem(R.id.mspromo).setVisible(true);
        nav_Menu.findItem(R.id.trservis).setVisible(false);
        nav_Menu.findItem(R.id.trhistory).setVisible(true);
        nav_Menu.findItem(R.id.logout).setVisible(true);
    }

    private void menuKary(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.msbarang).setVisible(false);
        nav_Menu.findItem(R.id.mskaryawan).setVisible(false);
        nav_Menu.findItem(R.id.mspelanggan).setVisible(false);
        nav_Menu.findItem(R.id.mspromo).setVisible(true);
        nav_Menu.findItem(R.id.trservis).setVisible(false);
        nav_Menu.findItem(R.id.trhistory).setVisible(true);
        nav_Menu.findItem(R.id.logout).setVisible(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected( MenuItem item) {
        int id = item.getItemId();
        Integer data = checkAndRequestPermissionsStatus();
        if(data.intValue()!=0){
            Toast.makeText(MainActivity.this, "Akses permission harap diterima semua!", Toast.LENGTH_SHORT).show();
        }else{
            if (id == R.id.msbarang) {
                startActivity(new Intent(MainActivity.this, MasterTipeBarang.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.mskaryawan) {
                startActivity(new Intent(MainActivity.this, MasterKaryawanActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.mspelanggan) {
                startActivity(new Intent(MainActivity.this, MasterUserActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.mspromo) {
                startActivity(new Intent(MainActivity.this, MasterPromoActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if(id == R.id.trhistory){
                if(status.equals("C")) {
                    startActivity(new Intent(MainActivity.this, TransHistCustActivity.class));
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else if(status.equals("K")) {
                    startActivity(new Intent(MainActivity.this, TransHistTeknisiActivity.class));
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else if(status.equals("A")) {
                    startActivity(new Intent(MainActivity.this, TransHistTeknisiActivity.class));
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    Toast.makeText(MainActivity.this, "INVALID STATUS USER", Toast.LENGTH_LONG).show();
                }
            } else if(id == R.id.trservis){
                Intent i  = new Intent(MainActivity.this, TransHistCustItemActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (id == R.id.logout) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(userId);
                pref.clear();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Integer checkAndRequestPermissionsStatus() {
        Integer a = 0;
        int writeExtStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readExtStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int lokasi = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int lokasi2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (writeExtStorage != PackageManager.PERMISSION_GRANTED) {
            a++;
        }
        if (readExtStorage != PackageManager.PERMISSION_GRANTED) {
            a++;
        }
        if (lokasi != PackageManager.PERMISSION_GRANTED) {
            a++;
        }
        if (lokasi2 != PackageManager.PERMISSION_GRANTED) {
            a++;
        }
        return a;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /*private void CheckGpsStatus(){
        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }*/
}