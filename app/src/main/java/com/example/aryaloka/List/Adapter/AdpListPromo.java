package com.example.aryaloka.List.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.aryaloka.List.Model.ListPromoModel;
import com.example.aryaloka.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;

public class AdpListPromo extends RecyclerView.Adapter<AdpListPromo.ViewHolder> implements Filterable {

    private Context context;
    private ArrayList<ListPromoModel> mArrayList;
    private ArrayList<ListPromoModel> mFilteredList;
    private DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);

    public AdpListPromo(Context contextku, ArrayList<ListPromoModel> arrayList) {
        context = contextku;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public AdpListPromo.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_list_promo, viewGroup, false);
        return new AdpListPromo.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdpListPromo.ViewHolder viewHolder, int i) {
        try{
            viewHolder.tv_kode.setText(mFilteredList.get(i).getKodePromo());
            viewHolder.tv_nama.setText(mFilteredList.get(i).getNamaPromo());
            viewHolder.tv_nilai.setText(formatter.format(mFilteredList.get(i).getNilai().setScale(0)));
        }catch(Exception ex){}
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<ListPromoModel> filteredList = new ArrayList<>();
                    for (ListPromoModel entity : mArrayList) {
                        if (entity.getNamaPromo().toLowerCase().contains(charString) ) {
                            filteredList.add(entity);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ListPromoModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv_kode,tv_nama, tv_nilai;

        public ViewHolder(View view) {
            super(view);
            tv_kode = (TextView)view.findViewById(R.id.txt_view_masterpromo_kode);
            tv_nama = (TextView)view.findViewById(R.id.txt_view_masterpromo_nama);
            tv_nilai = (TextView)view.findViewById(R.id.txt_view_masterpromo_nilai);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("kode", tv_kode.getText().toString());
            intent.putExtra("nama", tv_nama.getText().toString());
            intent.putExtra("nilai", tv_nilai.getText().toString());
            ((Activity)context).setResult(RESULT_OK, intent);
            ((Activity)context).finish();
        }
    }
}
