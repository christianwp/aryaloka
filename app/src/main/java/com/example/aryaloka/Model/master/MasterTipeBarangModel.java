package com.example.aryaloka.Model.master;

import java.io.Serializable;

public class MasterTipeBarangModel implements Serializable {

    private String kodeBarang;
    private String namabarang;


    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getNamabarang() {
        return namabarang;
    }

    public void setNamabarang(String namabarang) {
        this.namabarang = namabarang;
    }
}
