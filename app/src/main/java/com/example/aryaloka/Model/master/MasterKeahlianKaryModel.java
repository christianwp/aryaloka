package com.example.aryaloka.Model.master;

import java.io.Serializable;
import java.math.BigDecimal;

public class MasterKeahlianKaryModel implements Serializable {

    private String kodeKary;
    private String namaKary;
    private String kodeBarang;
    private String namaBarang;
    private BigDecimal nilai;
    private String cstat;

    public String getKodeKary() {
        return kodeKary;
    }

    public void setKodeKary(String kodeKary) {
        this.kodeKary = kodeKary;
    }

    public String getNamaKary() {
        return namaKary;
    }

    public void setNamaKary(String namaKary) {
        this.namaKary = namaKary;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public BigDecimal getNilai() {
        return nilai;
    }

    public void setNilai(BigDecimal nilai) {
        this.nilai = nilai;
    }

    public String getCstat() {
        return cstat;
    }

    public void setCstat(String cstat) {
        this.cstat = cstat;
    }
}
