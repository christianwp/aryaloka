package com.example.aryaloka.Model.transaksi;

import com.example.aryaloka.Model.master.MasterKaryawanModel;

import java.io.Serializable;
import java.math.BigDecimal;

public class TrHistCustModel implements Serializable {

    private String noTrans;
    private String tglTrans;
    private String kodeUserCust;
    private String namaUserCust;
    private String kodeTipeBarang;
    private String namaTipeBarang;
    private String kodePromo;
    private String namaPromo;
    private BigDecimal nilaiPromo;
    private String keluhan;
    private double latt;
    private double longt;
    private String alamat;
    private String kodeUserTeknisi;
    private String namaUserTeknisi;
    private String tglAmbil;
    private BigDecimal biayaPerkiraan;
    private Integer hariKerjaPerkiraan;
    private String ketTeknisi;
    private String tglSelesai;
    private BigDecimal biaya;
    private BigDecimal netto;
    private String tglAntar;
    private Integer hariGaransi;
    private String tglBatasGaransi;
    private String cdstatus;
    private String noReferensi;
    private Integer countUlasan;
    private Integer countUserUlasan;
    private MasterKaryawanModel karyModel;

    public String getNoTrans() {
        return noTrans;
    }

    public void setNoTrans(String noTrans) {
        this.noTrans = noTrans;
    }

    public String getTglTrans() {
        return tglTrans;
    }

    public void setTglTrans(String tglTrans) {
        this.tglTrans = tglTrans;
    }

    public String getKodeUserCust() {
        return kodeUserCust;
    }

    public void setKodeUserCust(String kodeUserCust) {
        this.kodeUserCust = kodeUserCust;
    }

    public String getNamaUserCust() {
        return namaUserCust;
    }

    public void setNamaUserCust(String namaUserCust) {
        this.namaUserCust = namaUserCust;
    }

    public String getKodeTipeBarang() {
        return kodeTipeBarang;
    }

    public void setKodeTipeBarang(String kodeTipeBarang) {
        this.kodeTipeBarang = kodeTipeBarang;
    }

    public String getNamaTipeBarang() {
        return namaTipeBarang;
    }

    public void setNamaTipeBarang(String namaTipeBarang) {
        this.namaTipeBarang = namaTipeBarang;
    }

    public String getKodePromo() {
        return kodePromo;
    }

    public void setKodePromo(String kodePromo) {
        this.kodePromo = kodePromo;
    }

    public String getNamaPromo() {
        return namaPromo;
    }

    public void setNamaPromo(String namaPromo) {
        this.namaPromo = namaPromo;
    }

    public BigDecimal getNilaiPromo() {
        return nilaiPromo;
    }

    public void setNilaiPromo(BigDecimal nilaiPromo) {
        this.nilaiPromo = nilaiPromo;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public double getLatt() {
        return latt;
    }

    public void setLatt(double latt) {
        this.latt = latt;
    }

    public double getLongt() {
        return longt;
    }

    public void setLongt(double longt) {
        this.longt = longt;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKodeUserTeknisi() {
        return kodeUserTeknisi;
    }

    public void setKodeUserTeknisi(String kodeUserTeknisi) {
        this.kodeUserTeknisi = kodeUserTeknisi;
    }

    public String getNamaUserTeknisi() {
        return namaUserTeknisi;
    }

    public void setNamaUserTeknisi(String namaUserTeknisi) {
        this.namaUserTeknisi = namaUserTeknisi;
    }

    public String getTglAmbil() {
        return tglAmbil;
    }

    public void setTglAmbil(String tglAmbil) {
        this.tglAmbil = tglAmbil;
    }

    public BigDecimal getBiayaPerkiraan() {
        return biayaPerkiraan;
    }

    public void setBiayaPerkiraan(BigDecimal biayaPerkiraan) {
        this.biayaPerkiraan = biayaPerkiraan;
    }

    public Integer getHariKerjaPerkiraan() {
        return hariKerjaPerkiraan;
    }

    public void setHariKerjaPerkiraan(Integer hariKerjaPerkiraan) {
        this.hariKerjaPerkiraan = hariKerjaPerkiraan;
    }

    public String getKetTeknisi() {
        return ketTeknisi;
    }

    public void setKetTeknisi(String ketTeknisi) {
        this.ketTeknisi = ketTeknisi;
    }

    public String getTglSelesai() {
        return tglSelesai;
    }

    public void setTglSelesai(String tglSelesai) {
        this.tglSelesai = tglSelesai;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public BigDecimal getNetto() {
        return netto;
    }

    public void setNetto(BigDecimal netto) {
        this.netto = netto;
    }

    public String getTglAntar() {
        return tglAntar;
    }

    public void setTglAntar(String tglAntar) {
        this.tglAntar = tglAntar;
    }

    public Integer getHariGaransi() {
        return hariGaransi;
    }

    public void setHariGaransi(Integer hariGaransi) {
        this.hariGaransi = hariGaransi;
    }

    public String getTglBatasGaransi() {
        return tglBatasGaransi;
    }

    public void setTglBatasGaransi(String tglBatasGaransi) {
        this.tglBatasGaransi = tglBatasGaransi;
    }

    public String getCdstatus() {
        return cdstatus;
    }

    public void setCdstatus(String cdstatus) {
        this.cdstatus = cdstatus;
    }

    public MasterKaryawanModel getKaryModel() {
        return karyModel;
    }

    public void setKaryModel(MasterKaryawanModel karyModel) {
        this.karyModel = karyModel;
    }

    public Integer getCountUlasan() {
        return countUlasan;
    }

    public void setCountUlasan(Integer countUlasan) {
        this.countUlasan = countUlasan;
    }

    public Integer getCountUserUlasan() {
        return countUserUlasan;
    }

    public void setCountUserUlasan(Integer countUserUlasan) {
        this.countUserUlasan = countUserUlasan;
    }

    public String getNoReferensi() {
        return noReferensi;
    }

    public void setNoReferensi(String noReferensi) {
        this.noReferensi = noReferensi;
    }
}
