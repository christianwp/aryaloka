package com.example.aryaloka.Model.master;

import java.io.Serializable;

public class MasterKaryawanModel implements Serializable {

    private String idKaryawan;
    private String namakary;
    private String tgllahir;
    private String telp;
    private String kota;
    private String pathFoto;

    public String getIdKaryawan() {
        return idKaryawan;
    }

    public void setIdKaryawan(String idKaryawan) {
        this.idKaryawan = idKaryawan;
    }

    public String getNamakary() {
        return namakary;
    }

    public void setNamakary(String namakary) {
        this.namakary = namakary;
    }

    public String getTgllahir() {
        return tgllahir;
    }

    public void setTgllahir(String tgllahir) {
        this.tgllahir = tgllahir;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getPathFoto() {
        return pathFoto;
    }

    public void setPathFoto(String pathFoto) {
        this.pathFoto = pathFoto;
    }
}
