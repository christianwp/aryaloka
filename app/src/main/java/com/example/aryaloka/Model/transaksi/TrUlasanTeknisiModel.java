package com.example.aryaloka.Model.transaksi;

import java.io.Serializable;

public class TrUlasanTeknisiModel implements Serializable {

    private String noTrans;
    private String kodeTeknisi;
    private String kodeCust;
    private String namaCust;
    private String text;
    private Integer bintang;

    public String getNoTrans() {
        return noTrans;
    }

    public void setNoTrans(String noTrans) {
        this.noTrans = noTrans;
    }

    public String getKodeTeknisi() {
        return kodeTeknisi;
    }

    public void setKodeTeknisi(String kodeTeknisi) {
        this.kodeTeknisi = kodeTeknisi;
    }

    public String getKodeCust() {
        return kodeCust;
    }

    public void setKodeCust(String kodeCust) {
        this.kodeCust = kodeCust;
    }

    public String getNamaCust() {
        return namaCust;
    }

    public void setNamaCust(String namaCust) {
        this.namaCust = namaCust;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getBintang() {
        return bintang;
    }

    public void setBintang(Integer bintang) {
        this.bintang = bintang;
    }
}
